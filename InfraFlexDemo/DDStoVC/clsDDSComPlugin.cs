﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using VisualComponents.Connectivity.Shared;
using VisualComponents.Create3D;
using Caliburn.Micro;
using Plugin.Connectivity.DDS_OpenSplice.Settings;

namespace Plugin.Connectivity.DDS_OpenSplice
{
    [Export(typeof(IConnectionPlugin))]
    [Export(typeof(clsDDSComPlugin))]
    [ExportMetadata("RequiredFeatures", LicenseFeatures.Features.Communication)]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class clsDDSComPlugin : PropertyChangedBase, IConnectionPlugin
    {

        private List<clsConnectionDDS> _oConnections;
        static clsDDSComPlugin Instance;

        public string Name => "DDS Communication";

        public object ExtraSettings { get => null; set { } }

        public IEnumerable<Type> PluginObjectPropertyTypes
        {
            get
            {
                Type[] test = { typeof(clsConnectionSettings), typeof(clsGroupSettings), typeof(clsValueItemID) };
                return test;
            }
        }

        public bool SupportsThreadPerServer => true;

        public bool SupportsThreadPerVariableGroup => true;

        public bool SupportsAsyncWrite => true;

        public bool SupportsAsyncRead => true;

        public IServer AddServer()
        {
            clsConnectionDDS oConnection = new clsConnectionDDS();
            _oConnections.Add(oConnection);
            return oConnection;
        }

        public bool RemoveServer(IServer serverToRemove)
        {
            if (serverToRemove == null) return false;
            clsConnectionDDS oConnection = serverToRemove as clsConnectionDDS;
            oConnection.Dispose();
            return _oConnections.Remove((clsConnectionDDS)serverToRemove);

        }

        [ImportingConstructor]
        public clsDDSComPlugin()
        {
            if (Instance != null)
            {
                throw new Exception("Only a single clsDDSComPlugin instance should be created. Use clsDDSComPlugin.Instance to get the existing instance.");
            }
            _oConnections = new List<clsConnectionDDS>();
            Instance = this;
        }
    }
}
