﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice
{
    class clsTreeNode : ITreeNode
    {
        #region Fields
        private string _sDisplayName;
        private ITreeNode _oParentNode;
        private List<ITreeNode> _oChildNodes;
        private IValueItem _oValueItem;
        private TreeNodeType _oType;
        #endregion

        public clsTreeNode(ITreeNode oParent, TreeNodeType oType, clsValueItem oValueItem)
        {
            _oParentNode = oParent;
            Type = oType;
            _oValueItem = oValueItem;
            if (_oValueItem != null) DisplayName = _oValueItem.DisplayName;
        }

        public TreeNodeType Type
        {
            get
            {
                return _oType;
            }
            set
            {
                _oType = value;
            }
        }

        public string DisplayName
        {
            get
            {
                return _sDisplayName;
            }
            set
            {
                _sDisplayName = value;
            }
        }

        public ITreeNode ParentNode
        {
            get
            {
                return _oParentNode;
            }
            set
            {
                _oParentNode = value;
            }
        }

        public bool HasChildren
        {
            get
            {
                return _oChildNodes != null && _oChildNodes.Count > 0;
            }
        }

        public IEnumerable<ITreeNode> ChildNodes
        {
            get
            {
                return _oChildNodes;
            }
            set
            {
                _oChildNodes = value.ToList();
            }
        }

        public IValueItem ValueItem
        {
            get
            {
                return _oValueItem;
            }
            set
            {
                _oValueItem = value;
            }
        }
    }
}
