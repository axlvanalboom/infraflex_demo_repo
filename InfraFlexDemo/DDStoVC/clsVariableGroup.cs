﻿using Caliburn.Micro;
using DataStructs;
using DDS;
using DDSToSOAP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice
{
    class clsVariableGroup : PropertyChangedBase, IVariableGroup
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(clsVariableGroup));

        private clsConnectionDDS _oConnection;
        private bool _xActive;
        List<clsValueItem> _oItems;
        Settings.clsGroupSettings _oGroupSettings;
        IDataWriter oWriter;

        public clsVariableGroup(clsConnectionDDS oConnection, VariableGroupDataDirection oFlowDirection, clsDDSParticipant oParticipant)
        {

            _oConnection = oConnection;
            DataFlowDirection = oFlowDirection;
            _oGroupSettings = new Settings.clsGroupSettings();
            _oItems = new List<clsValueItem>();

            if (DataFlowDirection == VariableGroupDataDirection.ServerToSimulation)
            {
                clsListener oListener = new clsListener();
                oListener.OnDataAvailableHandler += OListener_DataAvailableHandler;
                IDataReader oReader = oParticipant.addReader("Execution", new ExecutionTypeSupport(), oListener);
            }
            else
            {
                 oWriter = oParticipant.addWriter("Status", new StatusTypeSupport());
            }
        }

        private void OListener_DataAvailableHandler(IDataReader oReader)
        {
            ExecutionDataReader o = oReader as ExecutionDataReader;

            Execution[] arrMsg = null;
            SampleInfo[] arrInfo = null;
            o.Take(ref arrMsg, ref arrInfo);

            List<clsValueItem> changedItems = new List<clsValueItem>();
            foreach (Execution oMs in arrMsg)
            {
                foreach (clsValueItem item in _oItems.Where(x => x.ResourceID==oMs.ResourceId) )
                {
                    item.CachedValue = oMs.Parameter;
                    item.ValueSynchronizationTimestamp = Stopwatch.GetTimestamp();
                    item.Quality = ItemValueQuality.Good;
                    changedItems.Add(item);
                   
                }
            }
            if (ValueChangeEventsEnabled)
            {
                ValuesChanged(this, new ValuesChangedEventArgs(changedItems.ToArray()));
            }
            
        }

        public bool Active 
        { 
            get
            {
                return _xActive;
            }
            set
            {
                _xActive = value;
            }

        }

        public VariableGroupDataDirection DataFlowDirection
        {
            get;
            private set;
        }

        public object ExtraSettings { 
            get {
                return _oGroupSettings;
                    }
            set {
                _oGroupSettings = value as Settings.clsGroupSettings; 
            } 
        }
        public bool ValueChangeEventsEnabled {
            get;
            set;
        }

        public event EventHandler<ValuesChangedEventArgs> ValuesChanged;
        public event EventHandler<ServerOperationCompletedEventArgs> WriteToServerCompleted;
        public event EventHandler<ServerOperationCompletedEventArgs> ReadFromServerCompleted;

        public IValueItem AddItem(object valueItemID)
        {
            Settings.clsValueItemID valID = new Settings.clsValueItemID(valueItemID as Settings.clsValueItemID);
            bool ValidID = (valID.ResourceID != 0);
            int _iResourceID = 0;
            while (!ValidID)
            {
                string input = Microsoft.VisualBasic.Interaction.InputBox("Fill in the resourceID to filter", "ResourceID");
                if (int.TryParse(input, out _iResourceID))
                {
                    valID.ResourceID = _iResourceID;
                    ValidID = true;
                }
            };
            clsValueItem oItem = new clsValueItem(valID);
            _oItems.Add(oItem);
            return oItem;
        }

        public bool ReadFromServer(IEnumerable<IValueItem> itemsToRead)
        {
            return true;
        }

        public void ReadFromServerAsync(IEnumerable<IValueItem> itemsToRead, object userStateIdentifier)
        {
            Debug.Assert(ReadFromServerCompleted != null, "Calling server async read without having subscribed to the completion event.");

            //var valueItems = itemsToRead.Cast<ValueItem>().ToList();

            // Run the read operation using our own asynchronization mechanism. 
            // The async read and write methods would usually be provided by the communication libraries or system services used.
            Task.Run(() =>
            {
                try
                {
                    var success = ReadFromServer(itemsToRead);

                    // Tell connectivity core that the operation ran to completion. This event can be raised from any thread.
                    ReadFromServerCompleted(this, new ServerOperationCompletedEventArgs(null, false, userStateIdentifier, success));
                }
                catch (Exception ex)
                {
                    log.Error("Async execution of VariableGroup.ReadFromServer threw an exception.", ex);

                    // Tell connectivity core that the operation failed before completion. This event can be raised from any thread.
                    ReadFromServerCompleted(this, new ServerOperationCompletedEventArgs(ex, true, userStateIdentifier, false));
                }
            });
        }

        public bool RemoveItem(IValueItem item)
        {
            return _oItems.Remove(item as clsValueItem);
        }

        public bool WriteToServer(IEnumerable<IValueItem> itemsToWrite)
        {
            StatusDataWriter owr = oWriter as StatusDataWriter;
            foreach (IValueItem item in itemsToWrite)
                {
                clsValueItem valitem = item as clsValueItem;
                Status st = new Status() { ResourceId= valitem.ResourceID,  State= valitem.CachedValue.ToString() };
                owr.Write(st);
                valitem.ValueSynchronizationTimestamp = Stopwatch.GetTimestamp();
                valitem.Quality = ItemValueQuality.Good;
            }
            return true;

        }

        public void WriteToServerAsync(IEnumerable<IValueItem> itemsToWrite, object userStateIdentifier)
        {
            //var valueItems = itemsToWrite.Cast<ValueItem>().ToList();

            // Run the write operation using our own asynchronization mechanism. 
            // The async read and write methods would usually be provided by the communication libraries or system services used.
            Task.Run(() =>
            {
                try
                {
                    var success = WriteToServer(itemsToWrite);

                    // Tell connectivity core that the operation ran to completion. This event can be raised from any thread.
                    WriteToServerCompleted(this, new ServerOperationCompletedEventArgs(null, false, userStateIdentifier, success));
                }
                catch (Exception ex)
                {
                    log.Error("Async execution of VariableGroup.WriteToServer threw an exception.", ex);

                    // Tell connectivity core that the operation failed before completion. This event can be raised from any thread.
                    WriteToServerCompleted(this, new ServerOperationCompletedEventArgs(ex, true, userStateIdentifier, false));
                }
            });
        }
    }
}
