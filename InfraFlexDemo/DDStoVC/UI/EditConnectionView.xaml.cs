﻿using System.Windows.Controls;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    /// <summary>
    /// Interaction logic for EditConnectionView.xaml
    /// </summary>
    public partial class EditConnectionView : UserControl
    {
        public EditConnectionView()
        {
            InitializeComponent();
        }
    }
}
