﻿using System.Windows.Controls;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    /// <summary>
    /// Interaction logic for ServerPropertyPanel.xaml
    /// </summary>
    public partial class ServerPropertyPanel : UserControl
    {
        public ServerPropertyPanel()
        {
            InitializeComponent();
        }
    }
}
