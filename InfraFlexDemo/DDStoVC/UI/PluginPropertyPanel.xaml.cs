﻿using System.Windows.Controls;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    /// <summary>
    /// Interaction logic for PluginPropertyPanel.xaml
    /// </summary>
    public partial class PluginPropertyPanel : UserControl
    {
        public PluginPropertyPanel()
        {
            InitializeComponent();
        }
    }
}
