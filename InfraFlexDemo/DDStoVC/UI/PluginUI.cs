﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    [Export(typeof(IPluginUI))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    class PluginUI : IPluginUI
    {

        [Import]
#pragma warning disable CS0649 // Field 'PluginUI._oParentPlugin' is never assigned to, and will always have its default value null
        private clsDDSComPlugin _oParentPlugin;
#pragma warning restore CS0649 // Field 'PluginUI._oParentPlugin' is never assigned to, and will always have its default value null
        private PluginPropertyPanel _oPluginPropertyPanel;
        private ServerConnectionEditorUI _oEditConnetionUI;

        public IServerConnectionEditorUI EditConnectionUI
        {
            get
            {
                if (_oEditConnetionUI == null) _oEditConnetionUI = new ServerConnectionEditorUI();
                return _oEditConnetionUI;
            }
        }

        public IConnectionPlugin ParentPlugin => _oParentPlugin;

        public Control PluginConfigurationPanel
        {
            get
            {
                if (_oPluginPropertyPanel == null)
                {
                    _oPluginPropertyPanel = new PluginPropertyPanel();
                }
                return _oPluginPropertyPanel;
            }
        }

        public Control GetServerConfigurationPanel(IServer context)
        {
            return new ServerPropertyPanel() { DataContext = context };
        }

        public Control GetVariableGroupConfigurationPanel(IVariableGroup context)
        {
            return new VariableGroupConfigurationPanel() { DataContext = context };
        }
    }
}
