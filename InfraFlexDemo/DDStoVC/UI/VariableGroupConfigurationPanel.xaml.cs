﻿using System.Windows.Controls;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    /// <summary>
    /// Interaction logic for VariableGroupConfigurationPanel.xaml
    /// </summary>
    public partial class VariableGroupConfigurationPanel : UserControl
    {
        public VariableGroupConfigurationPanel()
        {
            InitializeComponent();
        }
    }
}
