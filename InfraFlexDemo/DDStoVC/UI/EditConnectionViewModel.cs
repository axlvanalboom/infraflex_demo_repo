﻿using Caliburn.Micro;
using System;
using System.Windows.Controls;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    class EditConnectionViewModel : PropertyChangedBase, IViewAware

    {
        private Control _oView;

        public Settings.clsConnectionSettings Settings { get; }

#pragma warning disable CS0067 // The event 'EditConnectionViewModel.ViewAttached' is never used
        public event EventHandler<ViewAttachedEventArgs> ViewAttached;
#pragma warning restore CS0067 // The event 'EditConnectionViewModel.ViewAttached' is never used

        public EditConnectionViewModel(Settings.clsConnectionSettings connSettings)
        {
            if (connSettings == null)
            {
                Settings = new Settings.clsConnectionSettings();
            }
            else
            {
                Settings = connSettings.Clone();
            }

            _oView = new EditConnectionView() { DataContext = this };
        }

        public void AttachView(object view, object context = null)
        {
            _oView = (Control)view;
        }

        public object GetView(object context = null)
        {
            return _oView;
        }
    }
}
