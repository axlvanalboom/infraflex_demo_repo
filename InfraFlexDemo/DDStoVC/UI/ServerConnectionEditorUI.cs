﻿using System;
using System.Windows.Controls;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice.UI
{
    class ServerConnectionEditorUI : IServerConnectionEditorUI
    {
        private EditConnectionViewModel viewModel;

        public Control PanelControl
        {
            get
            {
                if (viewModel == null) return null;
                return (Control)viewModel.GetView();
            }
        }

#pragma warning disable CS0067 // The event 'ServerConnectionEditorUI.ConnectionTestCompleted' is never used
        public event EventHandler<ConnectionTestCompletedEventArgs> ConnectionTestCompleted;
#pragma warning restore CS0067 // The event 'ServerConnectionEditorUI.ConnectionTestCompleted' is never used

        public void Cancel()
        {
            viewModel = null;
        }

        public IServerConnectionSettings Finish()
        {
            return viewModel.Settings;
        }

        public void Start(IServerConnectionSettings initialSettings)
        {
            viewModel = new EditConnectionViewModel((Settings.clsConnectionSettings)initialSettings);
        }

        public void TestConnectionAsync()
        {
          
        }
    }
}
