﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice
{
    class clsValueItem : IValueItem
    {

        #region Fields
        private string _sDescription;
        private Settings.clsValueItemID _oTagProperties;
        #endregion


        public clsValueItem(string Name, Type type, ValueItemAccess access)
        {
            _oTagProperties = new Settings.clsValueItemID(Name, type, access);
            IsValid = true;
        }

        public clsValueItem(Settings.clsValueItemID TagProperties)
        {
            _oTagProperties = TagProperties;
            IsValid = true;
        }

        public object ID => _oTagProperties;

        public string DisplayName
        {
            get
            {
                return _oTagProperties.DisplayName;
            }
            set
            {
                _oTagProperties.DisplayName = value;
            }
        }

        public bool IsValid { get; set; }

        public string Description
        {
            get
            {
                if (_sDescription == "") _sDescription = _oTagProperties.Address;
                return _sDescription;
            }
            set
            {
                _sDescription = value;
            }
        }

        public ItemValueQuality Quality {
            get;
            set;
        }

        public long ValueSynchronizationTimestamp { get; set; }

        public Type ValueType
        {
            get
            {
                return _oTagProperties.ValueType;
            }
            set
            {
                _oTagProperties.ValueType = value;
            }
        }

        public string ServerValueType
        {
            get
            {
                return _oTagProperties.ServerValueType;
            }
            set
            {
                _oTagProperties.ServerValueType = value;
            }
        }

        public ValueItemAccess Access {
            get
            {
                return _oTagProperties.Access;
            }
            set
            {
                _oTagProperties.Access = value;
            }

        }

        public object CachedValue { get; set; }

        public int ResourceID 
        { 
            get
            {
                return _oTagProperties.ResourceID;
            }
        }

        public override string ToString()
        {
            return _oTagProperties.ToString(); 
        }
    }
}
