﻿using Caliburn.Micro;
using System.Runtime.Serialization;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice.Settings
{
    [DataContract]
    class clsConnectionSettings : PropertyChangedBase, IServerConnectionSettings
    {
        public clsConnectionSettings()
        {

        }

        public bool ContainsAllRequiredInfo => true;

        public string DisplayIdentifier =>"";

        public clsConnectionSettings Clone()
        {
            return new clsConnectionSettings();
        }
    }
}
