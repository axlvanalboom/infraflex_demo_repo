﻿using Caliburn.Micro;
using System;
using System.Runtime.Serialization;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice.Settings
{
    [DataContract]
    class clsValueItemID : PropertyChangedBase
    {
        string _sValueType;

        [DataMember]
        public string DisplayName;
        [DataMember]
        public string ServerValueType
        {
            get { return _sValueType; }
            set { _sValueType = value;
                switch (_sValueType)
                {
                    case "System.String":
                        ValueType = typeof(string);
                        break;
                    default:
                        break;
                }
            }
        }

        [DataMember]
        public ValueItemAccess Access;

        public Type ValueType;

        [DataMember]
        public int ResourceID;

        public clsValueItemID(string Name, Type type, ValueItemAccess access)
        {
            DisplayName = Name;
            ValueType = type;
            ServerValueType = type.ToString();
            Access = access;
            ResourceID = 0;
        }

        public clsValueItemID(clsValueItemID original)
        {
            DisplayName = original.DisplayName;
            ValueType = original.ValueType;
            ServerValueType = original.ServerValueType;
            Access = original.Access;
            ResourceID = original.ResourceID;
        }

        public string Address
        {
            get
            {
                return DisplayName + ":" + ResourceID.ToString();
            }
        }

        public override string ToString()
        {
            return DisplayName + ":" + ResourceID.ToString();
        }
    }   
}
