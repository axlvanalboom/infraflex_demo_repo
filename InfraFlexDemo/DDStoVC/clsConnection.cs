﻿using Caliburn.Micro;
using DDSToSOAP;
using System;
using System.Collections.Generic;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.DDS_OpenSplice
{
    class clsConnectionDDS : PropertyChangedBase, IServer, IDisposable
    {
        readonly List<clsVariableGroup> _oVariableGroups;
        DataStructs.clsDDSParticipant oParticipant;
        Settings.clsConnectionSettings _oSettings;

        public clsConnectionDDS()
        {
            oParticipant = new DataStructs.clsDDSParticipant("Comment", "file://DDS//DDS_DefaultQoS_All.xml", "DDS DefaultQosProfile");
            _oVariableGroups = new List<clsVariableGroup>();
        }

        public bool Connected { get; private set; }

        public string DisplayName => "";

        public object ExtraSettings
        {
            get
            {
                return _oSettings;
            }
            set
            {
                if (value == null)
                {
                    _oSettings = new Settings.clsConnectionSettings();
                    NotifyOfPropertyChange();
                    return;
                }
                Settings.clsConnectionSettings newSettings = value as Settings.clsConnectionSettings;
                _oSettings = newSettings;
                NotifyOfPropertyChange();

            }
        }

        public ServerInfo? ServerSpecificInfo 
        {
            get
            {
                return new ServerInfo()
                {
                    MinTimeBetweenUpdates = TimeSpan.FromMilliseconds(1),
                    ProvidesValueChangeEvents = false,
                    SupportsStartStopReset = false,
                    SupportsAsyncRead = true,
                    SupportsAsyncWrite = true,
                    SupportsBatchReads = true,
                    SupportsBatchWrites = true
                };
            }
        }

        public event EventHandler TreeItemStructureChanged;
        public event EventHandler<ConnectionStateChangeEventArgs> ConnectionStateChanged;
        public event EventHandler<LogEventArgs> ServerLogEvent;

        public IVariableGroup AddVariableGroup(VariableGroupDataDirection dataFlowDirection)
        {
            clsVariableGroup NewGroup = new clsVariableGroup(this, dataFlowDirection, oParticipant);
            _oVariableGroups.Add(NewGroup);
            return NewGroup;
        }

        public IEnumerable<ITreeNode> Browse()
        {
            List<clsTreeNode> oItems = new List<clsTreeNode>
            {
                new clsTreeNode(null, TreeNodeType.ItemWithValue, new clsValueItem("Data", typeof(string), ValueItemAccess.ReadAndWrite))
            };
            List<ITreeNode> oTest = new List<ITreeNode> { new clsTreeNode(null, TreeNodeType.ItemGroup, null) { DisplayName = "Variables", ChildNodes = oItems } };

            return oTest;
        }

        public bool Connect(IServerConnectionSettings connectionSettings)
        {
            Connected = true;
            return true;
        }

        public void Disconnect()
        {
            Connected = false;
        }

        public void Dispose()
        {
            // Destroy it all
            oParticipant.destroy();
        }

        public bool RemoveVariableGroup(IVariableGroup groupToRemove)
        {
            bool removed = _oVariableGroups.Remove((clsVariableGroup)groupToRemove);
            if (removed) groupToRemove.Active = false;
            return removed;
        }

        public bool Reset()
        {
            return true;
        }

        public bool Start()
        {
            return true;
        }

        public bool Stop()
        {
            return true;
        }
    }
}
