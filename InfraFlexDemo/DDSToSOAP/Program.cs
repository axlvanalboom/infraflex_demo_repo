﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDS;
using DataStructs;

namespace DDSToSOAP
{
    class Program
    {
        //static StatusDataWriter _oWriter = null;

        //static void Main(string[] args)
        //{
        //    // Create participant
        //    clsDDSParticipant oParticipant = new clsDDSParticipant("DDSToSOAP", Properties.Settings.Default.QoS_File, Properties.Settings.Default.QoS_Profile);

        //    // Create delegate for read function
        //    clsListener oListener = new clsListener();
        //    oListener.OnDataAvailableHandler += OListener_DataAvailableHandler;

        //    // Add a reader
        //    //MsgDataReader oReader = oParticipant.addReader("HelloWorldData_Ms", new MsgTypeSupport(), oListener, "userID NOT BETWEEN %0 AND %1", new string[2] { "5", "10" }) as MsgDataReader;
        //    ExecutionDataReader oReader = oParticipant.addReader("Execution", new ExecutionTypeSupport(), oListener) as ExecutionDataReader;

        //    // Add a writer
        //    _oWriter = oParticipant.addWriter("Status", new StatusTypeSupport()) as StatusDataWriter;

        //    // Wait here
        //    Console.WriteLine("DDSToSOAP up and running, this will bounce back information to the Orchestrator! Hit enter to destroy the application.");
        //    Console.ReadLine();

        //    // Destroy it all
        //    oParticipant.destroy();
        //}

        //private static void OListener_DataAvailableHandler(IDataReader oReader)
        //{
        //    ExecutionDataReader ExecutionReader = oReader as ExecutionDataReader;

        //    Execution[] arrExecutions = null;
        //    SampleInfo[] arrInfo = null;
        //    ExecutionReader.Take(ref arrExecutions, ref arrInfo);

        //    foreach (Execution oExec in arrExecutions)
        //    {
        //        Console.WriteLine("----------------------------------------");
        //        Console.WriteLine("Incoming execution:");
        //        Console.WriteLine(oExec.ResourceId);
        //        Console.WriteLine(oExec.Parameter);

        //        Status oState = new Status();
        //        oState.ResourceId = oExec.ResourceId;
        //        oState.State = "Ok ok, I will do it";
        //        oState.Info = oExec.Parameter;
        //        _oWriter.Write(oState);

        //        Console.WriteLine("Sending back the status.");
        //        Console.WriteLine("----------------------------------------");
        //    }
        //}

        static void Main(string[] args)
        {
            var a = new TestProgrammaVoorExecution();
        }

    }
}
