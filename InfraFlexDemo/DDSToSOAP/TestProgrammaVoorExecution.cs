﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using DDS;
using DataStructs;
using Newtonsoft.Json.Linq;

namespace DDSToSOAP
{
    public class TestProgrammaVoorExecution
    {
        StatusDataWriter _oWriter;
        Status _oState;
        Random _oR;
        public TestProgrammaVoorExecution()
        {
            // Create participant
            clsDDSParticipant oParticipant = new clsDDSParticipant("TestProgrammaVoorExecution", Properties.Settings.Default.QoS_File, Properties.Settings.Default.QoS_Profile);

            // Create delegate for read function
            clsListener oListener = new clsListener();
            oListener.OnDataAvailableHandler += OListener_DataAvailableHandler;

            // Add a reader
            ExecutionDataReader oReader = oParticipant.addReader("Execution", new ExecutionTypeSupport(), oListener) as ExecutionDataReader;

            // Add a writer
            _oWriter = oParticipant.addWriter("Status", new StatusTypeSupport()) as StatusDataWriter;

            _oState = new Status();

            _oR = new Random();
            _oState.ResourceId = 1;

            _oState.State = "init";
            _oState.Info = "";
            _oWriter.Write(_oState);

            Thread.Sleep(2000); //2 seconden initializing

            _oState.State = "idle";
            _oWriter.Write(_oState);

            Console.ReadLine();

            oParticipant.destroy();
        }

        private void OListener_DataAvailableHandler(IDataReader oReader)
        {
            ExecutionDataReader ExecutionReader = oReader as ExecutionDataReader;

            Execution[] arrExecutions = null;
            SampleInfo[] arrInfo = null;
            ExecutionReader.Take(ref arrExecutions, ref arrInfo);

            foreach (Execution oExec in arrExecutions)
            {
                Console.WriteLine("----------------------------------------");
                Console.WriteLine("Incoming execution:");
                Console.WriteLine(oExec.ResourceId);
                Console.WriteLine(oExec.Parameter);

                if (_oState != null && _oState.State =="idle")
                {
                    ExecutingSkill(oExec.Parameter);
                }

            }
        }

        private void ExecutingSkill(string sParameter)
        {
            JObject json = JObject.Parse(sParameter);

            var oMove = json["Move"];
            var oManipulate = json["Manipulate"];
            var oMoveForce = json["MoveForce"];

            _oState.State = "busy";

            if (oMove != null) { _oState.Info = "Move"; }
            else if (oManipulate != null) { _oState.Info = "Manipulate"; }
            else if (oMoveForce != null) { _oState.Info = "MoveForce"; }

            _oWriter.Write(_oState);

            Thread.Sleep(_oR.Next(1000,5000));

            _oState.State = "idle";
            _oState.Info = "";
            _oWriter.Write(_oState);
        }

    }
}
