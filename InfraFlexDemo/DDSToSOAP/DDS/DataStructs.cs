using DDS;
using System.Runtime.InteropServices;

namespace DataStructs
{
    #region Status
    [StructLayout(LayoutKind.Sequential)]
    public sealed class Status
    {
        public int ResourceId;
        public string State = string.Empty;
        public string Info = string.Empty;
    };
    #endregion

    #region Execution
    [StructLayout(LayoutKind.Sequential)]
    public sealed class Execution
    {
        public int ResourceId;
        public string Parameter = string.Empty;
    };
    #endregion

}

