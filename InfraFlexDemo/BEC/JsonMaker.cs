﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BEC
{
    public class JsonMaker
    {
        private static string CreateJson(Object o) { return JsonConvert.SerializeObject(o); }
        public static string CreateJsonString(MoveJson o){ return CreateJson(o);}
        public static string CreateJsonString(ManipulateJson o){ return CreateJson(o);}
        public static string CreateJsonString(MoveForceJson o){ return CreateJson(o);}
        public static string CreateJsonString(DetectJson o) { return CreateJson(o); }
    }
}
