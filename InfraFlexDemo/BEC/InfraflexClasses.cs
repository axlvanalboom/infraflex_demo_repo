﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json.Converters;

namespace BEC
{
    public enum PoseType : Int16
    {
        Joint = 0,
        Circular = 1,
        Linear = 2
    }

    public enum MoveType : Int16
    {
        Pose = 0,
        Topic = 1
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ManipulateType : Int16
    {
        Open = 0,       // Simple opening
        Close = 1,      // Simple closing
        Binary = 2,     // Value 0 is open, 1 is close
        Absolute = 3,   // Value contains the absolute value for the gripper
        Relative = 4,   // Value container a relative value for the gripper
        ForceOpen = 5,  // Value contains the force value of the open gripper movement
        ForceClose = 6  // Value contains the force value of the close gripper movement
    }

    public enum ReferenceType : Int16
    {
        Absolute = 0,
        Relative = 1
    }

    public enum SourceType : Int16
    {
        Internal = 0,
        External = 1
    }

    [JsonConverter(typeof(StringEnumConverter))]
    //[Flags]
    public enum DetectType : Int16
    {
        Color = 0,
        Angle = 1,
        Position = 2,
        OCR = 3
    }

    public class clsPosition
    {
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        public string W { get; set; }
        public string P { get; set; }
        public string R { get; set; }
        public clsPosition(string fX, string fY, string fZ, string fW, string fP, string fR)
        {
            X = fX;
            Y = fY;
            Z = fZ;
            W = fW;
            P = fP;
            R = fR;
        }
        public clsPosition(string fX, string fY, string fZ)
        {
            X = fX;
            Y = fY;
            Z = fZ;
        }

        public clsPosition()
        {

        }

    }

    public class clsPose
    {
        public ReferenceType Reference { get; set; }
        public clsPosition Position { get; set; }
        public PoseType Type { get; set; }
        public double WayPointRadius { get; set; }
        public double Speed { get; set; }
        public double Acceleration { get; set; }
        public double Deceleration { get; set; }

        public clsPose(ReferenceType eReference, clsPosition oPosition, PoseType ePoseType, double fWayPointRadius, double fSpeed, double fAcceleration, double fDeceleration)
        {
            Reference = eReference;
            Position = oPosition;
            Type = ePoseType;
            WayPointRadius = fWayPointRadius;
            Speed = fSpeed;
            Acceleration = fAcceleration;
            Deceleration = fDeceleration;
        }

        public clsPose()
        {
            Position = new clsPosition();
        }
    }

    public class clsPart
    {
        public clsPosition CenterOfGravity { get; set; }
        public double Mass { get; set; }

        public clsPart(clsPosition oCenterOfGravity, double fMass)
        {
            CenterOfGravity = oCenterOfGravity;
            Mass = fMass;
        }

        public clsPart()
        {
            CenterOfGravity = new clsPosition();
        }
    }

    public class clsMove : clsSkillParameter
    {
        public clsPart Object { get; set; }
        public MoveType Type { get; set; }
        public int FrameworkId { get; set; }
        public int ToolId { get; set; }
        public bool Homing { get; set; }
        public List<clsPose> Poses { get; set; }
        public string TopicId { get; set; }

        public clsMove(clsPart oPart, MoveType eType, int iFrameworkId, int iToolId, bool xHoming, List<clsPose> oPoses, string sTopicId)
        {
            Object = oPart;
            Type = eType;
            FrameworkId = iFrameworkId;
            ToolId = iToolId;
            Homing = xHoming;
            Poses = oPoses;
            TopicId = sTopicId;
        }

        public clsMove()
        {
            Object = new clsPart();
            Poses = new List<clsPose>();
        }
    }

    public class clsGeneral
    {
        public ManipulateType Type { get; set; }
        public double Value { get; set; }

        public clsGeneral(ManipulateType eType)
        {
            if (eType != ManipulateType.Open && eType != ManipulateType.Close)
                throw new Exception("You must insert a value if not using the open or close type!");
            else
                Type = eType;
        }

        public clsGeneral(ManipulateType eType, double fValue)
        {
            Type = eType;
            Value = fValue;
        }

        public clsGeneral()
        {

        }
    }

    public class clsManipulate : clsSkillParameter
    {
        public clsPart Object { get; set; }
        public clsGeneral General { get; set; }

        public clsManipulate(clsPart oPart, clsGeneral oGeneral)
        {
            Object = oPart;
            General = oGeneral;
        }
        public clsManipulate()
        {
            Object = new clsPart();
            General = new clsGeneral();
        }
    }

    public class clsDetect : clsSkillParameter
    {
        public DetectType Type { get; set; }
        public int MaxObjects { get; set; }
        public double ExposureTime { get; set; }

        public clsDetect(DetectType oType = DetectType.Angle, int iMaxObjects = 1, double fExposureTime = 1.0)
        {
            Type = oType;
            MaxObjects = iMaxObjects;
            ExposureTime = fExposureTime;
        }

        public clsDetect() { }
    }

    public class clsRange
    {
        public double Minima { get; set; }
        public double Maxima { get; set; }
        public clsRange(double fMinima, double fMaxima)
        {
            Minima = fMinima;
            Maxima = fMaxima;
        }

        public clsRange()
        {

        }

    }

    public class clsForceVector
    {

        public clsRange X { get; set; }
        public clsRange Y { get; set; }
        public clsRange Z { get; set; }

        public clsForceVector(clsRange oX, clsRange oY, clsRange oZ)
        {
            X = oX;
            Y = oY;
            Z = oZ;
        }

        public clsForceVector()
        {
            X = new clsRange();
            Y = new clsRange();
            Z = new clsRange();
        }
    }

    public class clsForceSensing
    {

        public SourceType ForceOrigin { get; set; }
        public string ForceListeningId { get; set; }
        public clsForceSensing(SourceType sOrigin, string sListeningId = null)
        {
            ForceOrigin = sOrigin;
            ForceListeningId = sListeningId;
        }

        public clsForceSensing()
        {

        }

    }

    public class clsMoveForce : clsMove
    {
        public List<clsForceVector> ForceVectors { get; set; }
        public clsForceSensing ForceSensing { get; set; }

        public clsMoveForce(clsPart oPart, MoveType eType, int iFrameworkId, int iToolId, bool xHoming, List<clsPose> oPoses, string sTopicId, List<clsForceVector> oForceVectors, clsForceSensing oForceSensing)
            : base(oPart, eType, iFrameworkId, iToolId, xHoming, oPoses, sTopicId)
        {
            if (oPoses.Count != oForceVectors.Count) { throw new Exception("Amount of poses and forcevectors are not equal"); }
            ForceVectors = oForceVectors;
            ForceSensing = oForceSensing;
        }

        public clsMoveForce()
        {
            ForceSensing = new clsForceSensing();
            ForceVectors = new List<clsForceVector>();
        }
    }

    public abstract class JsonHelper { public virtual object getInnerObject() { return null; } }

    public class MoveJson : JsonHelper
    {
        public clsMove Move { get; set; }
        public MoveJson(clsMove oMove) { Move = oMove; }
        public MoveJson()
        {
            Move = new clsMove();
        }
        public override object getInnerObject() { return Move; }
    }
    public class ManipulateJson : JsonHelper
    {
        public clsManipulate Manipulate { get; set; }
        public ManipulateJson(clsManipulate oManipulate) { Manipulate = oManipulate; }
        public ManipulateJson() 
        {
            Manipulate = new clsManipulate();
        }
        public override object getInnerObject() { return Manipulate; }
    }
    public class MoveForceJson : JsonHelper
    {
        public clsMove MoveForce { get; set; }
        public MoveForceJson(clsMoveForce oMoveForce) { MoveForce = oMoveForce; }
        public MoveForceJson() 
        {
            MoveForce = new clsMoveForce();
        }
        public override object getInnerObject() { return MoveForce; }
    }

    public class DetectJson : JsonHelper 
    {
        public clsDetect Detect { get; set; }
        public DetectJson(clsDetect oDetect) { Detect = oDetect; }
        public DetectJson()
        {
            Detect = new clsDetect();
        }
        public override object getInnerObject() { return Detect; }
    }

    public class clsSkillParameter
    {
        public string Info { get; set; }
        public int ResourceId { get; set; }
        public int Counter { get; set; }
        public clsSkillParameter() { }
    }

    public class clsRecipe 
    {
        public string Name { get; set; }
        public List<clsSkillParameter> Skills { get; set; }

        public clsRecipe()
        {
            Skills = new List<clsSkillParameter>();
        }

        public clsRecipe(string sName, List<clsSkillParameter> lstSkills)
        {
            Name = sName;
            Skills = lstSkills;
        }
    }

    public class clsRecipeJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(clsRecipe).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject oJson = JObject.Load(reader);

            clsRecipe oReturn = new clsRecipe();
            oReturn.Name = oJson["Name"].ToString();
            oReturn.Skills = JsonConvert.DeserializeObject<List<clsSkillParameter>>(oJson["Skills"].ToString(), new clsSkillParametersJsonConverter());
            
            return oReturn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken oToken = JToken.FromObject(value);
            oToken.WriteTo(writer);
        }
    }

    public class clsSkillParametersJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(clsSkillParameter).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject oJson = JObject.Load(reader);

            object oIsMove = oJson["Move"];
            object oIsMoveForce = oJson["MoveForce"];
            object oIsManipulate = oJson["Manipulate"];
            object oIsDetect = oJson["Detect"];
            object oIsTimer = oJson["Timer"];

            JsonHelper oHelper = null;
            if (oIsMove != null)
                oHelper = JsonConvert.DeserializeObject<MoveJson>(oJson.ToString());
            else if (oIsMoveForce != null)
                oHelper = JsonConvert.DeserializeObject<MoveForceJson>(oJson.ToString());
            else if (oIsManipulate != null)
                oHelper = JsonConvert.DeserializeObject<ManipulateJson>(oJson.ToString());
            else if (oIsDetect != null)
                oHelper = JsonConvert.DeserializeObject<DetectJson>(oJson.ToString());
            else if (oIsTimer != null)
                oHelper = JsonConvert.DeserializeObject<TimerJson>(oJson.ToString());

            return oHelper.getInnerObject();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            object oObject = null;

            if (value.GetType() == typeof(clsMove))
                oObject = new MoveJson((clsMove)value);
            else if (value.GetType() == typeof(clsMoveForce))
                oObject = new MoveForceJson((clsMoveForce)value);
            else if (value.GetType() == typeof(clsManipulate))
                oObject = new ManipulateJson((clsManipulate)value);
            else if (value.GetType() == typeof(clsDetect))
                oObject = new DetectJson((clsDetect)value);
            else if (value.GetType() == typeof(clsTimer))
                oObject = new TimerJson((clsTimer)value);

            JToken oToken = JToken.FromObject(oObject);
            oToken.WriteTo(writer);
        }
    }

    public class clsRespons 
    {
        public string State { get; set; }
        public clsDetectRespons Info { get; set; }
        public clsRespons()
        {

        }
    }

    public class clsInfo 
    {
        public clsInfo()
        {

        }
    }

    public class clsDetectRespons : clsInfo
    {
        public bool Succeeded { get; set; }
        public double Time { get; set; }
        public List<clsDetectedObject> DetectedObjects { get; set; }
        public clsDetectRespons()
        {

        }
    }

    public class clsDetectResponsJson : JsonHelper
    {
        public clsDetectRespons DetectRespons { get; set; }
        public clsDetectResponsJson(clsDetectRespons oDetectRespons) { DetectRespons = oDetectRespons; }
        public clsDetectResponsJson()
        {
            DetectRespons = new clsDetectRespons();
        }
        public override object getInnerObject() { return DetectRespons; }
    }

    public class clsDetectedObject 
    {
        public string Color { get; set; }
        public double Angle { get; set; }
        public string OCR { get; set; }
        public clsPosition Position { get; set; }

        public clsDetectedObject()
        {

        }

    }

    public class clsTimer : clsSkillParameter
    {
        public string Tag { get; set; }
        public bool StartStop { get; set; }
        public clsTimer()
        {

        }
    }

    public class TimerJson : JsonHelper
    {
        public clsTimer Timer { get; set; }
        public TimerJson(clsTimer oTimer) { Timer = oTimer; }
        public TimerJson()
        {
            Timer = new clsTimer();
        }
        public override object getInnerObject() { return Timer; }
    }

    public class clsLog 
    {
        public int Mode { get; set; }
        public double VelocityRate { get; set; }
        public double Duration { get; set; }
        public string TaskId { get; set; }
        public clsLog(string sTaskId, int xMode, double fDuration)
        {
            TaskId = sTaskId;
            Mode = xMode;
            Duration = fDuration;
            VelocityRate = 100.0;
        }
    }
}
