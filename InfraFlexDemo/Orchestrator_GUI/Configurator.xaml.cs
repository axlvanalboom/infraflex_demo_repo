﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Orchestrator;
using BEC;
using Orchestrator_GUI.UserControls;
using Microsoft.Win32;

namespace Orchestrator_GUI
{
    /// <summary>
    /// Interaction logic for Configurator.xaml
    /// </summary>
    public partial class Configurator : Window
    {
        clsSkillOrchestrator _oOrchestrator;
        public Configurator(clsSkillOrchestrator oOrchestrator)
        {
            InitializeComponent();
            _oOrchestrator = oOrchestrator;
            DrawSkillList();
        }

        private void DrawSkillList(int iIndex=0)
        {
            if (iIndex <0) { iIndex = 0; }
            lstSkillList.ItemsSource = null;

            List<clsSkillParameter> oSkills = _oOrchestrator.getSkills();
            List<string> oTypes = _oOrchestrator.getSkillTypes();
            List<string> oShow = new List<string>();
            foreach (clsSkillParameter oParam in oSkills)
            {
                string sResult = "";
                if (oParam.GetType() == typeof(clsMove)) { sResult = "Move : "; }
                else if (oParam.GetType() == typeof(clsManipulate)) { sResult = "Manipulate : "; }
                else if (oParam.GetType() == typeof(clsMoveForce)) { sResult = "MoveForce : "; }
                else if (oParam.GetType() == typeof(clsDetect)) { sResult = "Detect : "; }
                else if (oParam.GetType() == typeof(clsTimer)) { sResult = "Timer : "; }
                sResult += oParam.Info;
                oShow.Add(sResult);
            }
            txtRecipe.Text = _oOrchestrator.getRecipeName();
            lstSkillList.ItemsSource = oShow;
            lstSkillList.SelectedIndex = iIndex;
            
        }
        private void DrawSkill(int iIndex)
        {
            object oSkill = _oOrchestrator.getSkillParameter(iIndex);
            UserControl Draw;
            bool xEnable = _oOrchestrator.getSkills().Count == 0;

            switch (lstSkillList.SelectedItem.ToString().Split(':')[0].Trim())
            {
                case "Move":
                    Draw = new ucMove((clsMove)oSkill);
                    ((ucMove)Draw).EditMode(xEnable);
                    break;
                case "Manipulate":
                    Draw = new ucManipulate((clsManipulate)oSkill);
                    ((ucManipulate)Draw).EditMode(xEnable);
                    break;
                case "MoveForce":
                    Draw = new ucMoveForce((clsMoveForce)oSkill);
                    ((ucMoveForce)Draw).EditMode(xEnable);
                    break;
                case "Detect":
                    Draw = new ucDetect((clsDetect)oSkill);
                    ((ucDetect)Draw).EditMode(xEnable);
                    break;
                case "Timer":
                    Draw = new ucTimer((clsTimer)oSkill, getActiveTimerTags(iIndex));
                    ((ucTimer)Draw).EditMode(xEnable);
                    break;
                default: throw new Exception("Type not implemented!");
            }

            txtInfo.IsEnabled = xEnable;
            txtResourceId.IsEnabled = xEnable;

            txtInfo.Text = ((clsSkillParameter)oSkill).Info;
            txtResourceId.Text = ((clsSkillParameter)oSkill).ResourceId.ToString();

            #region "Draw"
            Grid grid = new Grid();
            grid.Margin = new Thickness(0, 0, 0, 0);
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.HorizontalAlignment = HorizontalAlignment.Left;

            Draw.VerticalAlignment = VerticalAlignment.Top;
            Draw.HorizontalAlignment = HorizontalAlignment.Left;

            grid.Children.Add(Draw);
            
            //grid.Height = Draw.Height + 50;
            //grid.Width = Draw.Width + 50;
            gpbSkillParameters.Content = grid;
            //gpbSkillParameters.Width = Draw.Width + 100;
            //gpbSkillParameters.Height = Draw.Height + 100;
            #endregion 
        }

        private List<string> getActiveTimerTags(int iSkillIndex) 
        {
            List<clsSkillParameter> lstSkills = _oOrchestrator.getSkills();
            List<string> lstActiveTags = new List<string>();
            for (int i = 0; i < iSkillIndex; i++) 
            {
                if (lstSkills[i].GetType() == typeof(clsTimer)) 
                {
                    clsTimer oTimer = (clsTimer)lstSkills[i];
                    if (oTimer.StartStop)
                        lstActiveTags.Add(oTimer.Tag);
                    else
                        lstActiveTags.RemoveAll(x => x == oTimer.Tag);
                }
            }

            return lstActiveTags;
        }

        public delegate void saveEvent(clsSkillOrchestrator Orchestrator);
        public event saveEvent Save;
        private void SaveSettings_Click(object sender, RoutedEventArgs e)
        {
            SaveSettingsEvent();
            this.Close();
        }
        private void SaveSettingsEvent()
        {
            saveEvent save = Save;
            save?.Invoke(_oOrchestrator);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var oTypes = _oOrchestrator.getSkillTypes();
            Adding wpfAdd = new Adding(oTypes, getActiveTimerTags(lstSkillList.SelectedIndex));
            wpfAdd.Add += Add;
            wpfAdd.IsClosing += AddingClosing;
            this.IsEnabled = false;
            wpfAdd.Show();
        }
        private void Add(clsSkillParameter sender)
        {
            this.IsEnabled = true;
            if (_oOrchestrator.addSkill(sender, lstSkillList.SelectedIndex + 1)) { DrawSkillList(lstSkillList.SelectedIndex + 1); }
        }
        private void AddingClosing(object sender, EventArgs e)
        {
            this.IsEnabled = true;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = lstSkillList.SelectedIndex;
            if (iIndex < 0) { return; }
            _oOrchestrator.deleteSkill(iIndex);
            DrawSkillList(iIndex - 1);
        }

        private void btnSwapUp_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = lstSkillList.SelectedIndex;
            if (iIndex == 0) { return; }
            _oOrchestrator.swapSkillParameters(iIndex, iIndex - 1);
            DrawSkillList(iIndex - 1);
        }
        private void btnSwapDown_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = lstSkillList.SelectedIndex;
            if (iIndex == _oOrchestrator.getSkills().Count - 1) { return; }
            _oOrchestrator.swapSkillParameters(iIndex, iIndex + 1);
            DrawSkillList(iIndex + 1);
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (ofd.ShowDialog() == true)
            {
                _oOrchestrator.loadListFromFile(ofd.FileName);
                //this.Dispatcher.Invoke(new Action(delegate { DrawSkillList(); }));
                //Application.Current.Dispatcher.Invoke(new Action(() => { DrawSkillList(); }));
                DrawSkillList();
            }         
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (sfd.ShowDialog() == true)
            {
                _oOrchestrator.saveListToFile(sfd.FileName);
            }        
        }

        private void btnChangeParameters_Click(object sender, RoutedEventArgs e)
        {
            if (btnChangeParameters.Content.ToString() == "Edit Parameters")
            {
                clsSkillParameter oParameter = _oOrchestrator.getSkills()[lstSkillList.SelectedIndex];
                txtInfo.Text = oParameter.Info;
                txtResourceId.Text = oParameter.ResourceId.ToString();

                txtInfo.IsEnabled = true;
                txtResourceId.IsEnabled = true;

                Grid grid = (Grid)gpbSkillParameters.Content;
                object uc = grid.Children[0];

                switch (lstSkillList.SelectedItem.ToString().Split(':')[0].Trim())
                {
                    case "Move":
                        var ucMove = (ucMove)uc;
                        ucMove.EditMode(true);
                        break;
                    case "Manipulate":
                        var ucMani = (ucManipulate)uc;
                        ucMani.EditMode(true);
                        break;
                    case "MoveForce":
                        var ucMoveForce = (ucMoveForce)uc;
                        ucMoveForce.EditMode(true);
                        break;
                    case "Detect":
                        var ucDetect = (ucDetect)uc;
                        ucDetect.EditMode(true);
                        break;
                    case "Timer":
                        var ucTimer = (ucTimer)uc;
                        ucTimer.EditMode(true);
                        break;
                    default: throw new Exception("Type not implemented!");
                }
                 
                btnChangeParameters.Content = "Save Parameters";
            }
            else if (btnChangeParameters.Content.ToString() == "Save Parameters")
            {
                Grid grid = (Grid)gpbSkillParameters.Content;
                object uc = grid.Children[0];
                int iResourceId = 0;
                Int32.TryParse(txtResourceId.Text, out iResourceId);

                txtInfo.IsEnabled = false;
                txtResourceId.IsEnabled = false;

                switch (lstSkillList.SelectedItem.ToString().Split(':')[0].Trim())
                {
                    case "Move":
                        var ucMove = (ucMove)uc;
                        ucMove.EditMode(false);
                        var oMove = ucMove.getMove();
                        oMove.ResourceId = iResourceId;
                        oMove.Info = txtInfo.Text;
                        _oOrchestrator.editSkill(oMove, lstSkillList.SelectedIndex);
                        break;
                    case "Manipulate":
                        var ucMani = (ucManipulate)uc;
                        ucMani.EditMode(false);
                        var oMani = ucMani.getManipulate();
                        oMani.ResourceId = iResourceId;
                        oMani.Info = txtInfo.Text;
                        _oOrchestrator.editSkill(oMani, lstSkillList.SelectedIndex);
                        break;
                    case "MoveForce":
                        var ucMoveForce = (ucMoveForce)uc;
                        ucMoveForce.EditMode(false);
                        var oMoveForce = ucMoveForce.getMoveForce();
                        oMoveForce.ResourceId = iResourceId;
                        oMoveForce.Info = txtInfo.Text;
                        _oOrchestrator.editSkill(oMoveForce, lstSkillList.SelectedIndex);
                        break;
                    case "Detect":
                        var ucDetect = (ucDetect)uc;
                        ucDetect.EditMode(false);
                        var oDetect = ucDetect.getDetect();
                        oDetect.ResourceId = iResourceId;
                        oDetect.Info = txtInfo.Text;
                        _oOrchestrator.editSkill(oDetect, lstSkillList.SelectedIndex);
                        break;
                    case "Timer":
                        var ucTimer = (ucTimer)uc;
                        ucTimer.EditMode(false);
                        var oTimer = ucTimer.getTimer();
                        oTimer.ResourceId = iResourceId;
                        oTimer.Info = txtInfo.Text;
                        _oOrchestrator.editSkill(oTimer, lstSkillList.SelectedIndex);
                        break;
                    default: throw new Exception("Type not implemented!");
                }
                btnChangeParameters.Content = "Edit Parameters";

                DrawSkillList(lstSkillList.SelectedIndex);
                //lstSkillList.SelectedItem = lstSkillList.SelectedItem.ToString().Split(':')[0].Trim() + " : " + txtInfo.Text; 
            }
        }

        private void lstSkillList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnChangeParameters.Content = "Edit Parameters";
            if (lstSkillList.ItemsSource == null) { return; }
            DrawSkill(lstSkillList.SelectedIndex);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.IsEnabled) { e.Cancel = true; return; }
            SaveSettingsEvent();
        }

        private void txtResourceId_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsInt(txtResourceId.Text))
            {
                txtResourceId.Text = "";
            }
        }

        private void txtRecipe_TextChanged(object sender, TextChangedEventArgs e)
        {
            _oOrchestrator.editRecipeName(txtRecipe.Text);
        }
    }
}
