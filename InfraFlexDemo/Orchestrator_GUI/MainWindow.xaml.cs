﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;
using Orchestrator_GUI.UserControls;
using Orchestrator;
using DataStructs;
using System.Drawing;

namespace Orchestrator_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        clsSkillOrchestrator _oOrchestrator;
        //TestData _oOrchestrator;
        //TestData0 _oBackEnd;

        private bool _xForced = false;
        //private List<string> _oTypes;

        public MainWindow()
        {
            InitializeComponent();
            _oOrchestrator = new clsSkillOrchestrator();
            _oOrchestrator.loadRecipesFromFile();
            _oOrchestrator.State += State;
            _oOrchestrator.Done += Done;
            _oOrchestrator.Step += Step;
            _oOrchestrator.ChangeRecipe += _oOrchestrator_ChangeRecipe;
            cmbRecipeId.ItemsSource = _oOrchestrator.getRecipes();
            if (cmbRecipeId.HasItems)
                cmbRecipeId.SelectedIndex = _oOrchestrator.RecipeId();

            DrawSkillList();
        }

        private void _oOrchestrator_ChangeRecipe(int iRecipeId, List<clsSkillParameter> lstSkills)
        {
            _xForced = true;

            Application.Current.Dispatcher.Invoke(() =>
            {
                cmbRecipeId.SelectedIndex = iRecipeId;
                DrawSkillList(lstSkills);
            });

            _xForced = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.IsEnabled) { e.Cancel = true; return; }
            _oOrchestrator.saveRecipesToFile();
            _oOrchestrator.Stop();
            _oOrchestrator.destroy();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            _oOrchestrator.Start(_iStartIndex);
            btnStart.IsEnabled = false;
            btnStop.IsEnabled = true;
            btnPauze.IsEnabled = true;
            btnResume.IsEnabled = false;
            btnConfigurator.IsEnabled = false;

            List<ListBoxItem> oItems = lstSkillList.Items.OfType<ListBoxItem>().ToList();
            for (int i = 0; i < oItems.Count; i++)
            {
                if (i == _iStartIndex)
                {
                    oItems[i].Background = System.Windows.Media.Brushes.LightBlue;
                }
                else
                {
                    oItems[i].Background = System.Windows.Media.Brushes.White;
                }
            }
            lstSkillList.ItemsSource = oItems;
            lstSkillList.SelectedIndex = -1;
        }
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            _oOrchestrator.Stop(true);
            btnStart.IsEnabled = true;
            btnStop.IsEnabled = false;
            btnPauze.IsEnabled = false;
            btnResume.IsEnabled = false;
            btnConfigurator.IsEnabled = true;

            List<ListBoxItem> oItems = lstSkillList.Items.OfType<ListBoxItem>().ToList();
            for (int i = 0; i < oItems.Count; i++){ oItems[i].Background = System.Windows.Media.Brushes.White; }
            lstSkillList.ItemsSource = oItems;
        }

        private void btnPauze_Click(object sender, RoutedEventArgs e)
        {
            _oOrchestrator.Pause();
            btnStart.IsEnabled = false;
            btnStop.IsEnabled = true;
            btnPauze.IsEnabled = false;
            btnResume.IsEnabled = true;
        }
        private void btnResume_Click(object sender, RoutedEventArgs e)
        {
            _oOrchestrator.Resume();
            btnStart.IsEnabled = false;
            btnStop.IsEnabled = true;
            btnPauze.IsEnabled = true;
            btnResume.IsEnabled = false;
        }

        private void btnConfigurator_Click(object sender, RoutedEventArgs e)
        {
            Configurator wpfConfig = new Configurator(_oOrchestrator);
            wpfConfig.Save += Save;
            this.IsEnabled = false;
            wpfConfig.Show();
        }
        private void Save(clsSkillOrchestrator orchestrator)
        {
            _oOrchestrator = orchestrator;
            this.IsEnabled = true;
            cmbRecipeId.ItemsSource = _oOrchestrator.getRecipes();
            cmbRecipeId.SelectedIndex = _oOrchestrator.RecipeId();
            DrawSkillList();
        }

        private void DrawSkillList(List<clsSkillParameter> lstSkills = null)
        {
            lstSkillList.ItemsSource = null;

            List<clsSkillParameter> oSkills;

            if (lstSkills == null)
            {
                oSkills = _oOrchestrator.getSkills();
                //_oTypes = _oOrchestrator.getSkillTypes();
            }
            else
                oSkills = lstSkills;

            List<ListBoxItem> oShow = new List<ListBoxItem>();

            foreach (clsSkillParameter oParam in oSkills)
            {
                string sResult = "";
                if (oParam.GetType() == typeof(clsMove)) { sResult = "Move : "; }
                else if (oParam.GetType() == typeof(clsManipulate)) { sResult = "Manipulate : "; }
                else if (oParam.GetType() == typeof(clsMoveForce)) { sResult = "MoveForce : "; }
                else if (oParam.GetType() == typeof(clsDetect)) { sResult = "Detect : "; }
                else if (oParam.GetType() == typeof(clsTimer)) { sResult = "Timer : "; }
                sResult += oParam.Info;

                var oItem = new ListBoxItem(); 
                oItem.Content = sResult;
                oShow.Add(oItem);     
            }
             lstSkillList.ItemsSource = oShow;
        }
        
        int _iStartIndex;
        private void lstSkillList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstSkillList.SelectedIndex < 0) { _iStartIndex = 0; } 
            else { _iStartIndex = lstSkillList.SelectedIndex; }
        }

        //events

        public void State(Tuple<Status, DateTime> oStateAndUpdateTime)
        {
            Application.Current.Dispatcher.Invoke( new Action(() => { ShowState(oStateAndUpdateTime); }));
        }
        public void ShowState(Tuple<Status, DateTime> oStateAndUpdateTime)
        {
            lblStatusBar.Content = "Last State : " + oStateAndUpdateTime.Item1 + " | Received at: " + oStateAndUpdateTime.Item2;
        }

        public void Done()
        {
            Application.Current.Dispatcher.Invoke(new Action(() => { ShowDone(); }));
        }
        public void ShowDone()
        {
            btnStart.IsEnabled = true;
            btnStop.IsEnabled = false;
            btnPauze.IsEnabled = false;
            btnResume.IsEnabled = false;
            btnConfigurator.IsEnabled = true;

            List<ListBoxItem> oItems = lstSkillList.Items.OfType<ListBoxItem>().ToList();
            for (int i = 0; i < oItems.Count; i++)
            {
                oItems[i].Background = System.Windows.Media.Brushes.White;
            }
            lstSkillList.ItemsSource = oItems;
        }

        public void Step(Tuple<Execution, DateTime> oTuple, int iStep)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => { ShowStep(oTuple, iStep); }));
        }
        public void ShowStep(Tuple<Execution, DateTime> oTuple, int iStep)
        {
            List<ListBoxItem> oItems = lstSkillList.Items.OfType<ListBoxItem>().ToList();
            for (int i = 0; i < oItems.Count; i++)
            {
                if (i == iStep)
                {
                    oItems[i].Background = System.Windows.Media.Brushes.LightBlue;
                }
                else
                {
                    oItems[i].Background = System.Windows.Media.Brushes.White;
                }
            }
            lstSkillList.ItemsSource = oItems;
        }

        private void btnAddRecipe_Click(object sender, RoutedEventArgs e)
        {
            _oOrchestrator.addRecipe();
            btnConfigurator.IsEnabled = true;
            Configurator wpfConfig = new Configurator(_oOrchestrator);
            wpfConfig.Save += Save;
            this.IsEnabled = false;
            wpfConfig.Show();
        }

        private void cmbRecipeId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_xForced && cmbRecipeId.SelectedIndex >= 0)
            {
                _oOrchestrator.setRecipe(cmbRecipeId.SelectedIndex);
                DrawSkillList();
            }
        }

        private void btnDeleteRecipe_Click(object sender, RoutedEventArgs e)
        {
            _oOrchestrator.deleteRecipe(cmbRecipeId.SelectedIndex);
            cmbRecipeId.ItemsSource = _oOrchestrator.getRecipes();
            if (cmbRecipeId.HasItems)
                cmbRecipeId.SelectedIndex = _oOrchestrator.RecipeId();
            else
                btnConfigurator.IsEnabled = false;
        }

        private void btnAutoStart_Click(object sender, RoutedEventArgs e)
        {
            if (!_oOrchestrator.Running())
            {
                _oOrchestrator.StartAutoloop();
                btnAutoStart.IsEnabled = false;
                btnAutoStop.IsEnabled = true;

                btnStart_Click(new object(), new RoutedEventArgs());
            }
        }

        private void btnAutoStop_Click(object sender, RoutedEventArgs e)
        {
            if (_oOrchestrator.Running())
            {
                _oOrchestrator.StopAutoloop();
                btnAutoStart.IsEnabled = true;
                btnAutoStop.IsEnabled = false;
            }
        }
    }

}
