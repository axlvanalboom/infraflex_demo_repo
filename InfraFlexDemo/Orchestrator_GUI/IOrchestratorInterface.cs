﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEC;
using DataStructs;

namespace Orchestrator
{
    public delegate void StatusEventHandler(Tuple<Status, DateTime> oStateAndUpdateTime);
    public delegate void RecipeChangeHandler(int iRecipeId, List<clsSkillParameter> lstSkills);
    public delegate void ExecutionStepHandler(Tuple<Execution, DateTime> oExecutionAndTimeSent,int iStep);
    public delegate void ExecutionDoneHandler();
    public interface IOrchestratorInterface
    {
        // Methods and events
        bool Start(int iStartIndex = 0);
        bool Resume();
        bool Pause();
        bool Stop(bool xReturnToFirstStep = false);

        event StatusEventHandler State;
        event ExecutionStepHandler Step;
        event ExecutionDoneHandler Done;
        event RecipeChangeHandler ChangeRecipe;

        // Skill parameter management
        bool addSkill(clsSkillParameter oSkillParameter, int iInsertAtIndex = -1);
        bool editSkill(clsSkillParameter oSkillParameter, int iEditAtIndex);
        bool deleteSkill(int iDeleteAtIndex);
        bool swapSkillParameters(int iIndex1, int iIndex2);
        List<clsSkillParameter> getSkills();
        clsSkillParameter getSkillParameter(int iIndex);

        // Skill parameter setup (need to see what is exactly required for the GUI)
        List<string> getSkillTypes();

        // Save to file, read from file
        string saveListToFile(string sFileName = "SkillParameters.txt");
        string loadListFromFile(string sFileName = "SkillParameters.txt");
    }
}
