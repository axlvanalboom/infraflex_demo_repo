﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Orchestrator_GUI.UserControls;
using BEC;
namespace Orchestrator_GUI
{
    /// <summary>
    /// Interaction logic for Adding.xaml
    /// </summary>
    public partial class Adding : Window
    {
        public delegate void addEvent(clsSkillParameter SkillParameter);
        public event addEvent Add;

        private List<string> _lstTags;

        public Adding(List<string> oTypes, List<string> lstTags = null)
        {
            InitializeComponent();
            lstTypes.ItemsSource = oTypes;
            _lstTags = lstTags;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            
            string sType = lstTypes.SelectedItem.ToString();

            clsSkillParameter o;

            Grid grid = (Grid)spControl.Children[0];
            object uc = grid.Children[0];

            switch (sType)
            {
                case "Move":
                    var ucMove = (ucMove)uc;
                    o = ucMove.getMove();
                    break;
                case "Manipulate":
                    var ucMani = (ucManipulate)uc;
                    o = ucMani.getManipulate();
                    break;
                case "MoveForce":
                    var ucMoveForce = (ucMoveForce)uc;
                    o = ucMoveForce.getMoveForce();
                    break;
                case "Detect":
                    var ucDetect = (ucDetect)uc;
                    o = ucDetect.getDetect();
                    break;
                case "Timer":
                    var ucTimer = (ucTimer)uc;
                    o = ucTimer.getTimer();
                    break;
                default: throw new Exception("Type not implemented!");

            }
            o.Info = txtInfo.Text;

            int iResourceId = 0;
            Int32.TryParse(txtResourceId.Text, out iResourceId);
            o.ResourceId = iResourceId;

            addEvent add = Add;
            add?.Invoke(o);
            this.Close();
        }

        private void lstTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnAdd.IsEnabled = true;
            txtInfo.Visibility = Visibility.Hidden;
            lblInfo.Visibility = Visibility.Hidden;
            txtResourceId.Visibility = Visibility.Hidden;
            lblResourceId.Visibility = Visibility.Hidden;
            ListBox oList = (ListBox)sender;
            switch (oList.SelectedItem)
            {
                case "Move": DrawMove(); break;
                case "Manipulate": DrawManipulate(); break;
                case "MoveForce": DrawMoveForce(); break;
                case "Detect": DrawDetect(); break;
                case "Timer": DrawTimer(); break;
                default: throw new Exception("Type not implemented!");
            }        
        }

        private void DrawMove()
        {
            txtInfo.Visibility = Visibility.Visible;
            lblInfo.Visibility = Visibility.Visible;
            txtResourceId.Visibility = Visibility.Visible;
            lblResourceId.Visibility = Visibility.Visible;
            spControl.Children.Clear();
            Grid grid = new Grid();
            ucMove uc = new ucMove();
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            spControl.Children.Add(grid);
        }
        private void DrawManipulate()
        {
            txtInfo.Visibility = Visibility.Visible;
            lblInfo.Visibility = Visibility.Visible;
            txtResourceId.Visibility = Visibility.Visible;
            lblResourceId.Visibility = Visibility.Visible;
            spControl.Children.Clear();
            Grid grid = new Grid();
            ucManipulate uc = new ucManipulate();
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            spControl.Children.Add(grid);
        }
        private void DrawMoveForce()
        {
            txtInfo.Visibility = Visibility.Visible;
            lblInfo.Visibility = Visibility.Visible;
            txtResourceId.Visibility = Visibility.Visible;
            lblResourceId.Visibility = Visibility.Visible;
            spControl.Children.Clear();
            Grid grid = new Grid();
            ucMoveForce uc = new ucMoveForce();
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            spControl.Children.Add(grid);
        }

        private void DrawDetect()
        {
            txtInfo.Visibility = Visibility.Visible;
            lblInfo.Visibility = Visibility.Visible;
            txtResourceId.Visibility = Visibility.Visible;
            lblResourceId.Visibility = Visibility.Visible;
            spControl.Children.Clear();
            Grid grid = new Grid();
            ucDetect uc = new ucDetect();
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            spControl.Children.Add(grid);
        }

        private void DrawTimer()
        {
            txtInfo.Visibility = Visibility.Visible;
            lblInfo.Visibility = Visibility.Visible;
            txtResourceId.Visibility = Visibility.Visible;
            lblResourceId.Visibility = Visibility.Visible;
            spControl.Children.Clear();
            Grid grid = new Grid();
            ucTimer uc = new ucTimer(new clsTimer(), _lstTags);
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            spControl.Children.Add(grid);
        }

        public event EventHandler IsClosing;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EventHandler closing = IsClosing;
            closing?.Invoke(this, new EventArgs());
        }

        private void txtResourceId_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsInt(txtResourceId.Text))
            {
                txtResourceId.Text = "";
            }
        }
    }
}
