﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfluxDB;
using InfluxDB.LineProtocol.Payload;
using InfluxDB.LineProtocol.Client;
using InfluxData.Net.InfluxDb;
using InfluxData.Net.Common.Enums;
using BEC;

namespace Orchestrator
{
    public class clsInfluxDbFunctions
    {
        private LineProtocolClient _oWClient;
        private InfluxDbClient _oQClient;

        public clsInfluxDbFunctions(string sUri, string sDb)
        {
            _oWClient = new LineProtocolClient(new Uri(sUri), sDb);
            //_oQClient = new InfluxDbClient(sUri,"usr", "pwd", InfluxDbVersion.Latest);
        }

        public void LogData(clsLog oLog) 
        {
            Dictionary<string, object> dicValues = new Dictionary<string, object>();

            dicValues.Add("duration", oLog.Duration);
            dicValues.Add("mode", oLog.Mode);
            dicValues.Add("velocity_rate", oLog.VelocityRate);

            LineProtocolPayload oPayload = new LineProtocolPayload();          

            oPayload.Add(new LineProtocolPoint($"assembly_time_{oLog.TaskId}", dicValues));

            try
            {
                _oWClient.WriteAsync(oPayload);
                Console.WriteLine($"Succesfully logged data to InfluxDB");
            }
            catch (Exception e) 
            {
                Console.WriteLine($"Could not log data to InfluxDB");
                Console.WriteLine(e.Message);
            }


        }

        public int ReadCurrentRecipe() 
        {

            var query = "SELECT flag_mode FROM mode GROUP BY * ORDER BY DESC LIMIT 1";
            //var response = await _oQClient.Client.QueryAsync(query, "yourDbName");

            return 0; 
        }
    }
}
