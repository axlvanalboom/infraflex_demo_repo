﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucPoses.xaml
    /// </summary>
    public partial class  ucPoses : UserControl
    {
        List<clsPose> _oPoses; 

        public ucPoses()
        {
            InitializeComponent();
        }
        public ucPoses(List<clsPose> oPoses)
        {
            InitializeComponent();
            setPoses(oPoses);
        }

        public void setPoses(List<clsPose> oPoses)
        {
            List<string> oItems = new List<string>();
            if (oPoses !=null) 
            { 
                for (int i = 0; i < oPoses.Count; i++)
                {
                    oItems.Add(i.ToString());
                }
            }

            cmbSelector.ItemsSource = oItems;

            _oPoses = oPoses;
            cmbSelector.SelectedIndex = 0;
            spPoses.IsEnabled = false;

            btnAdd.Visibility = Visibility.Hidden;
            btnDelete.Visibility = Visibility.Hidden;
            btnSave.Visibility = Visibility.Hidden;
            btnSwapUp.Visibility = Visibility.Hidden;
            btnSwapDown.Visibility = Visibility.Hidden;

        }
        public List<clsPose> getPoses()
        {
            return _oPoses;
        }

        bool _xEdit;

        private void AddMode(bool add)
        {
            if (add) 
            {
                btnAdd.Visibility = Visibility.Visible;
                btnSave.Visibility = Visibility.Visible;
                btnSwapUp.Visibility = Visibility.Visible;
                btnSwapDown.Visibility = Visibility.Visible;
                btnDelete.Visibility = Visibility.Visible;
            }
            else 
            { 
                btnAdd.Visibility = Visibility.Hidden;
                btnSave.Visibility = Visibility.Hidden;
                btnSwapUp.Visibility = Visibility.Hidden;
                btnSwapDown.Visibility = Visibility.Hidden;
                btnDelete.Visibility = Visibility.Hidden;
            }
        }

        public void EditMode(bool edit)
        {
            AddMode(edit);
            _xEdit = edit;
          
            spPoses.IsEnabled = edit;
            btnSave.IsEnabled = edit;
            if (_oPoses != null && _oPoses.Count > 0)
            {
                if (cmbSelector.SelectedIndex < 0)
                {
                    cmbSelector.SelectedIndex = 0;
                }
                DrawPose(_oPoses[cmbSelector.SelectedIndex]);
            }
        }

        private void DrawPose(clsPose oPose)
        {
            spPoses.Children.Clear();

            int iX = 5;
            int iY = 35;

            Grid grid = new Grid();
            grid.Margin = new Thickness(0, 0, 0, 0);
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.HorizontalAlignment = HorizontalAlignment.Left;

            ucPose uc = new ucPose(oPose);
            uc.EditMode(_xEdit);
            uc.Margin = new Thickness(iX, iY, 0, 0);
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;

            grid.Children.Add(uc);

            
            //lstPoses.Content = grid;
            //grid.Width = uc.Width;
            //grid.Height = uc.Height;
            spPoses.Children.Add(grid);
            
            //spPoses.Width = grid.Width+10;
            //spPoses.Height = grid.Height+20;

            //gpbPoses.Width = spPoses.Width+5;
            //gpbPoses.Height = spPoses.Height+5;
            //View.Height = gpbPoses.Height+10;
            //View.Width = gpbPoses.Width+10;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox a = (ComboBox)sender;
            if (a.SelectedIndex >= 0)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => { DrawPose(_oPoses[a.SelectedIndex]); }));
                //DrawPose(_oPoses[a.SelectedIndex]);
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            btnAdd.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnSwapUp.IsEnabled = false;
            btnSwapDown.IsEnabled = false;
            btnDelete.IsEnabled = false;

            spPoses.Children.Clear();

            int iX = 5;
            int iY = 35;

            Grid grid = new Grid();
            ucPose uc = new ucPose();

            uc.EditMode(_xEdit);
            uc.Margin = new Thickness(iX, iY, 0, 0);
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.HorizontalAlignment = HorizontalAlignment.Left;
            //lstPoses.Content = grid;
            //grid.Width = uc.Width + 10;
            //grid.Height = uc.Height + 70;
            spPoses.Children.Add(grid);

            //spPoses.Width = grid.Width + 10;
            //spPoses.Height = grid.Height + 20;

            //gpbPoses.Width = spPoses.Width + 10;
            //gpbPoses.Height = spPoses.Height + 20;
            //View.Height = gpbPoses.Height + 10;
            //View.Width = gpbPoses.Width + 10;
            if (_oPoses == null) { _oPoses = new List<clsPose>(); }
            int iIndex = cmbSelector.SelectedIndex;
            _oPoses.Insert(iIndex + 1, uc.getPose());
            List<string> oItems = new List<string>();
            for (int i = 0; i < _oPoses.Count; i++)
            {
                oItems.Add(i.ToString());
            }
            cmbSelector.ItemsSource = oItems;
            cmbSelector.SelectedIndex = iIndex + 1;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            btnAdd.IsEnabled = true;
            //btnSave.IsEnabled = false;
            btnSwapUp.IsEnabled = true;
            btnSwapDown.IsEnabled = true;
            btnDelete.IsEnabled = true;

            Grid grid = (Grid)spPoses.Children[0];
            ucPose uc = (ucPose)grid.Children[0];
            clsPose oPose = uc.getPose();
            _oPoses[cmbSelector.SelectedIndex] = oPose;
                                 
           // if (_oPoses == null) { _oPoses = new List<clsPose>(); }
            

            //int iIndex = cmbSelector.SelectedIndex;
            //_oPoses.Insert(iIndex + 1, uc.getPose());
            //List<string> oItems = new List<string>();
            //for (int i = 0; i < _oPoses.Count; i++)
            //{
            //    oItems.Add(i.ToString());
            //}
            //cmbSelector.ItemsSource = oItems;
            //cmbSelector.SelectedIndex = iIndex +1;

        }

        private void btnSwapUp_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = cmbSelector.SelectedIndex;
            if (iIndex == 0) { return; }
            var oPose = _oPoses[iIndex];
            _oPoses.RemoveAt(iIndex);
            _oPoses.Insert(iIndex - 1,oPose);
            cmbSelector.SelectedIndex = iIndex - 1;
        }

        private void btnSwapDown_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = cmbSelector.SelectedIndex;
            if (iIndex >= _oPoses.Count-1) { return; }
            var oPose = _oPoses[iIndex];
            _oPoses.RemoveAt(iIndex);
            _oPoses.Insert(iIndex + 1,oPose);
            cmbSelector.SelectedIndex = iIndex + 1;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = cmbSelector.SelectedIndex;
            if (_oPoses.Count == 0 || iIndex >_oPoses.Count - 1) { return; }
            _oPoses.RemoveAt(iIndex);

            Grid grid = (Grid)spPoses.Children[0];
            ucPose uc = (ucPose)grid.Children[0];
            if (_oPoses == null) { _oPoses = new List<clsPose>(); }

            if (_oPoses.Count == 0) 
            {
                cmbSelector.ItemsSource = null;
                cmbSelector.SelectedIndex = -1;
                spPoses.Children.Clear();
                return; 
            }
            iIndex = cmbSelector.SelectedIndex-1;
            if (iIndex <0) { iIndex = 0; }
            List<string> oItems = new List<string>();
            
            for (int i = 0; i < _oPoses.Count; i++)
            {
                oItems.Add(i.ToString());
            }
            cmbSelector.ItemsSource = oItems;
            cmbSelector.SelectedIndex = iIndex;
        }

    }
}
