﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucGeneral.xaml
    /// </summary>
    public partial class ucGeneral : UserControl
    {
        public ucGeneral()
        {
            InitializeComponent();

            cmbType.ItemsSource = Enum.GetNames(typeof(ManipulateType));
            cmbType.SelectedIndex = 0;
        }

        public ucGeneral(clsGeneral oGeneral)
        {
            InitializeComponent();
            setGeneral(oGeneral);
        }

        public void setGeneral(clsGeneral oGeneral)
        {
            cmbType.ItemsSource = Enum.GetNames(typeof(ManipulateType));
            cmbType.SelectedItem = oGeneral.Type.ToString();

            txtValue.Text = oGeneral.Value.ToString();

            gpbGeneral.IsEnabled = false;
        }
        public clsGeneral getGeneral()
        {
            try
            {
                double fValue;// = double.Parse(txtValue.Text); 
                double.TryParse(txtValue.Text.Replace(".", ","), out fValue);
                ManipulateType mtType = (ManipulateType)Enum.Parse(typeof(ManipulateType), cmbType.SelectedItem.ToString());

                return new clsGeneral(mtType,fValue);

            }
            catch (Exception e) { throw new Exception("An input is wrong!"); }
        }

        public void EditMode(bool edit)
        {
            gpbGeneral.IsEnabled = edit;
        }


        private void txtValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtValue.Text))
            {
                txtValue.Text = "";
            }
        }
    }
}
