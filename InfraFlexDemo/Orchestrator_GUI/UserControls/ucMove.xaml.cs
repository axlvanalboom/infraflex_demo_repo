﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucMove.xaml
    /// </summary>
    public partial class ucMove : UserControl
    {
        public ucMove()
        {
            InitializeComponent();
            txtTopic.Text = "";
            cmbType.ItemsSource = Enum.GetNames(typeof(MoveType));
            cmbType.SelectedIndex = 0;
            cmbHoming.ItemsSource = new List<string>() { "True", "False" };
            cmbHoming.SelectedIndex = 0;
            EditMode(true);
        }

        public ucMove(clsMove oMove)
        {
            InitializeComponent();
            setMove(oMove);
        }

        public void setMove(clsMove oMove)
        {
            txtFrameworkId.Text = oMove.FrameworkId.ToString();
            txtToolId.Text = oMove.ToolId.ToString();
            if (oMove.TopicId == null) { oMove.TopicId = ""; }
            txtTopic.Text = oMove.TopicId.ToString();

            cmbType.ItemsSource = Enum.GetNames(typeof(MoveType));
            cmbType.SelectedItem = oMove.Type.ToString();

            cmbHoming.ItemsSource = new List<string>() { "True", "False" };
            cmbHoming.SelectedItem = oMove.Homing.ToString();

            PART.SetPart(oMove.Object);
            ucposesPoses.setPoses(oMove.Poses);
            
            //gpbMove.Height = PART.Height + 20 + ucposesPoses.Height;
            //Move.Height = PART.Height + 20 + ucposesPoses.Height;
            //MoveGrid.Height = PART.Height + 20 + ucposesPoses.Height;

            EditMode(false);
        }
        public clsMove getMove()
        {
            int iFramework; //= int.Parse(txtFrameworkId.Text);
            int.TryParse(txtFrameworkId.Text, out iFramework);
            int iTool;// = int.Parse(txtToolId.Text);
            int.TryParse(txtToolId.Text, out iTool);


            MoveType mtType = (MoveType)Enum.Parse(typeof(MoveType), cmbType.SelectedItem.ToString());

            bool xHoming = Convert.ToBoolean(cmbHoming.SelectedItem.ToString());

            return new clsMove(PART.getPart(), mtType, iFramework, iTool, xHoming, ucposesPoses.getPoses(), txtTopic.Text);
            
        }

        public void EditMode(bool edit)
        {
            txtFrameworkId.IsEnabled = edit;
            txtToolId.IsEnabled = edit;
            cmbHoming.IsEnabled = edit;
            cmbType.IsEnabled = edit;
            PART.EditMode(edit);
            ucposesPoses.EditMode(edit);
            txtTopic.IsEnabled = edit;
        //    gpbMove.IsEnabled = true;
        //    Move.IsEnabled = true;
        //    MoveGrid.IsEnabled = true;
        }

        private void txtFrameworkId_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsInt(txtFrameworkId.Text))
            {
                txtFrameworkId.Text = "";
            }
        }
        private void txtToolId_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsInt(txtToolId.Text))
            {
                txtToolId.Text = "";
            }
        }

        


    }
}
