﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucPosition.xaml
    /// </summary>
    public partial class ucPosition : UserControl
    {
        public ucPosition()
        {
            InitializeComponent(); 
        }

        public ucPosition(clsPosition oPosition)
        {
            InitializeComponent();
            SetPosition(oPosition);
        }

        public void SetPosition(clsPosition oPosition)
        {
            txtX.Text = oPosition.X.ToString();
            txtY.Text = oPosition.Y.ToString();
            txtZ.Text = oPosition.Z.ToString();
            txtW.Text = oPosition.W.ToString();
            txtP.Text = oPosition.P.ToString();
            txtR.Text = oPosition.R.ToString();
            gpbPosition.IsEnabled = false;
        }

        public void EditMode(bool edit)
        {
            gpbPosition.IsEnabled = edit;
        }

        public clsPosition GetPosition()
        {
            try
            {
                //double fX;// = double.Parse(txtX.Text);
                //double fY;//= double.Parse(txtY.Text);
                //double fZ;//= double.Parse(txtZ.Text);
                //double fW;//= double.Parse(txtW.Text);
                //double fP;//= double.Parse(txtP.Text);
                //double fR;//= double.Parse(txtR.Text);

                //double.TryParse(txtX.Text.Replace(".",","), out fX);
                //double.TryParse(txtY.Text.Replace(".",","), out fY);
                //double.TryParse(txtZ.Text.Replace(".",","), out fZ);
                //double.TryParse(txtW.Text.Replace(".",","), out fW);
                //double.TryParse(txtP.Text.Replace(".",","), out fP);
                //double.TryParse(txtR.Text.Replace(".", ","), out fR);

                //return new clsPosition(fX.ToString(), fY.ToString(), fZ.ToString(), fW.ToString(), fP.ToString(), fR.ToString());



                txtX.Text.Replace(".", ",");
                txtY.Text.Replace(".", ",");
                txtZ.Text.Replace(".", ",");
                txtW.Text.Replace(".", ",");
                txtP.Text.Replace(".", ",");
                txtR.Text.Replace(".", ",");

                return new clsPosition(txtX.Text, txtY.Text, txtZ.Text, txtW.Text, txtP.Text, txtR.Text);
            }
            catch (Exception e) { throw new Exception("An input is wrong!"); }
        }

        public void setName(string sName)
        {
            gpbPosition.Header = sName;
        }

        private void txtX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtX.Text))
            {
                txtX.Text = "0";
            }
        }
        private void txtY_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtY.Text))
            {
                txtY.Text = "0";
            }
        }
        private void txtZ_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtZ.Text))
            {
                txtZ.Text = "0";
            }
        }
        private void txtW_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtW.Text))
            {
                txtW.Text = "0";
            }
        }
        private void txtP_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtP.Text))
            {
                txtP.Text = "0";
            }
        }
        private void txtR_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtR.Text))
            {
                txtR.Text = "0";
            }
        }
    }

    
}
