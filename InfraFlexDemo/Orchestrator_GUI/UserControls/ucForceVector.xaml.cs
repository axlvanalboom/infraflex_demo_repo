﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucForceVector.xaml
    /// </summary>
    public partial class ucForceVector : UserControl
    {
        public ucForceVector()
        {
            InitializeComponent();
            setHeaders();
        }
        public ucForceVector(clsForceVector oForceVector)
        {
            InitializeComponent();
            setHeaders();
            setForceVector(oForceVector);
        }

        public void setHeaders()
        {
            X.setHeader("X");
            Y.setHeader("Y");
            Z.setHeader("Z");
        }

        public void setForceVector(clsForceVector oForceVector)
        {
            X.setRange(oForceVector.X);
            Y.setRange(oForceVector.Y);
            Z.setRange(oForceVector.Z);
            gpbForceVector.IsEnabled = false;
        }
        public clsForceVector getForceVector()
        {
            return new clsForceVector(X.getRange(), Y.getRange(), Z.getRange());
        }

        public void EditMode(bool edit)
        {
            gpbForceVector.IsEnabled = !edit;
            X.EditMode(!edit);
            Y.EditMode(!edit);
            Z.EditMode(!edit);
        }

    }
}
