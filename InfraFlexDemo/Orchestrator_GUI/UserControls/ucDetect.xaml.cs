﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucDetect.xaml
    /// </summary>
    public partial class ucDetect : UserControl
    {
        public ucDetect()
        {
            InitializeComponent();
            cmbDetectType.ItemsSource = Enum.GetNames(typeof(DetectType));
            cmbDetectType.SelectedIndex = 0;
            txtMaxObjects.Text = "1";
            txtExposureTime.Text = "1";
        }

        public ucDetect(clsDetect oDetect)
        {
            InitializeComponent();
            cmbDetectType.ItemsSource = Enum.GetNames(typeof(DetectType));
            setDetect(oDetect);
        }

        public void EditMode(bool xEdit) 
        {
            cmbDetectType.IsEnabled = xEdit;
            txtMaxObjects.IsEnabled = xEdit;
            txtExposureTime.IsEnabled = xEdit;
        }

        public clsDetect getDetect()
        {
            int iMaxObjects = 0;
            Int32.TryParse(txtMaxObjects.Text, out iMaxObjects);

            double fExposureTime = 0.0;
            Double.TryParse(txtExposureTime.Text, out fExposureTime);

            DetectType oType = (DetectType)Enum.Parse(typeof(DetectType), cmbDetectType.SelectedItem.ToString());

            return new clsDetect(oType, iMaxObjects, fExposureTime);
        }

        public void setDetect(clsDetect oDetect)
        {
            cmbDetectType.SelectedItem = oDetect.Type.ToString();
            txtMaxObjects.Text = oDetect.MaxObjects.ToString();
            txtExposureTime.Text = oDetect.ExposureTime.ToString();
        }

        private void txtMaxObjects_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsInt(txtMaxObjects.Text))
            {
                txtMaxObjects.Text = "";
            }
        }

        private void txtExposureTime_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtMaxObjects.Text))
            {
                txtExposureTime.Text = "";
            }
        }

        private void cmbDetectType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
