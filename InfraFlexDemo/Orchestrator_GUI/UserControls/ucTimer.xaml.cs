﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    public partial class ucTimer : UserControl
    {
        private List<string> _lstStartedTags;

        public ucTimer()
        {
            InitializeComponent();
            cmbType.ItemsSource = new List<string>() { "Start", "Stop"};
            cmbType.SelectedIndex = 0;
            cmbTag.Visibility = Visibility.Hidden;
            txtTag.Visibility = Visibility.Visible;
            txtTag.Text = "";
            _lstStartedTags = new List<string>();
        }

        public ucTimer(clsTimer oTimer, List<string> lstStartedTags)
        {
            InitializeComponent();
            _lstStartedTags = lstStartedTags;
            cmbTag.ItemsSource = lstStartedTags;
            cmbType.ItemsSource = new List<string>() { "Start", "Stop" };
            cmbType.SelectedIndex = oTimer.StartStop ? 0 : 1;
            txtTag.Text = oTimer.Tag;
            if (oTimer.Tag.Length > 0)
                cmbTag.SelectedItem = oTimer.Tag;
            toggleStartStop(oTimer.StartStop);
        }

        private void toggleStartStop(bool xStartStop) 
        {
            if (xStartStop)
            {
                cmbTag.Visibility = Visibility.Hidden;
                txtTag.Visibility = Visibility.Visible;
            }
            else
            {
                cmbTag.Visibility = Visibility.Visible;
                txtTag.Visibility = Visibility.Hidden;
            }
        }

        public clsTimer getTimer()
        {
            clsTimer oTimer = new clsTimer();
            oTimer.StartStop = (cmbType.SelectedIndex == 0);
            if (oTimer.StartStop)
                oTimer.Tag = txtTag.Text;
            else
                oTimer.Tag = (string)cmbTag.SelectedItem;

            return oTimer;
        }

        public void EditMode(bool xEdit)
        {
            cmbType.IsEnabled = xEdit;
            cmbTag.IsEnabled = xEdit;
            txtTag.IsEnabled = xEdit;
        }

        private void cmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            toggleStartStop(cmbType.SelectedIndex == 0);
        }
    }
}
