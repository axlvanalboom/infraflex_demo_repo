﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucPose.xaml
    /// </summary>
    public partial class ucPose : UserControl
    {
        public ucPose()
        {
            InitializeComponent();

            cmbRef.ItemsSource = Enum.GetNames(typeof(ReferenceType));
            cmbRef.SelectedIndex = 0;

            cmbType.ItemsSource = Enum.GetNames(typeof(PoseType));
            cmbType.SelectedIndex = 0;
        }
        public ucPose(clsPose oPose)
        {
            InitializeComponent();
            setPose(oPose);      
        }

        public void setPose(clsPose oPose)
        {
            txtWayPointRadius.Text = oPose.WayPointRadius.ToString();
            txtSpeed.Text = oPose.Speed.ToString();
            txtAcceleration.Text = oPose.Acceleration.ToString();
            txtDeceleration.Text = oPose.Deceleration.ToString();
            ucposiPosition.SetPosition(oPose.Position);

            cmbRef.ItemsSource = Enum.GetNames(typeof(ReferenceType));
            cmbRef.SelectedItem = oPose.Reference.ToString();

            cmbType.ItemsSource = Enum.GetNames(typeof(PoseType));
            cmbType.SelectedItem = oPose.Type.ToString();
            gpbPose.IsEnabled = false;

        }
        public clsPose getPose()
        {

            try
            {
                double fWPR;// = double.Parse(txtWayPointRadius.Text);
                double fSpe;// = double.Parse(txtSpeed.Text);
                double fAcc;// = double.Parse(txtAcceleration.Text);
                double fDec;// = double.Parse(txtDeceleration.Text);

                double.TryParse(txtWayPointRadius.Text.Replace(".", ","), out fWPR);
                double.TryParse(txtSpeed.Text.Replace(".", ","), out fSpe);
                double.TryParse(txtAcceleration.Text.Replace(".", ","), out fAcc);
                double.TryParse(txtDeceleration.Text.Replace(".", ","), out fDec);

                ReferenceType rtRef = (ReferenceType)Enum.Parse(typeof(ReferenceType), cmbRef.SelectedItem.ToString());
                PoseType mtType = (PoseType)Enum.Parse(typeof(PoseType), cmbType.SelectedItem.ToString());

                return new clsPose(rtRef, ucposiPosition.GetPosition(), mtType, fWPR, fSpe, fAcc, fDec);
                
            }
            catch (Exception e) { throw new Exception("An input is wrong!"); }
        }

        public void EditMode(bool edit)
        {
            ucposiPosition.EditMode(edit);
            gpbPose.IsEnabled = edit;
        }


        private void txtWayPointRadius_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtWayPointRadius.Text))
            {
                txtWayPointRadius.Text = "";
            }
        }
        private void txtSpeed_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtSpeed.Text))
            {
                txtSpeed.Text = "";
            }
        }
        private void txtAcceleration_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtAcceleration.Text))
            {
                txtAcceleration.Text = "";
            }
        }
        private void txtDeceleration_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtDeceleration.Text))
            {
                txtDeceleration.Text = "";
            }
        }
    }
}
