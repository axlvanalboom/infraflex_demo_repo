﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucForceVectors.xaml
    /// </summary>
    public partial class ucForceVectors : UserControl
    {
        List<clsForceVector> _oForceVectors;

        public ucForceVectors()
        {
            InitializeComponent();
        }
        public ucForceVectors(List<clsForceVector> oForceVectors)
        {
            InitializeComponent();
            setForceVectors(oForceVectors);
        }

        public void setForceVectors(List<clsForceVector> oForceVectors)
        {
            List<string> oItems = new List<string>();
            if (oForceVectors != null)
                for (int i = 0; i < oForceVectors.Count; i++) { oItems.Add(i.ToString()); }

            cmbSelector.ItemsSource = oItems;

            _oForceVectors = oForceVectors;
            cmbSelector.SelectedIndex = 0;

            EditMode(false);
        }
        public List<clsForceVector> getForceVectors()
        {
            return _oForceVectors;
        }

        bool _xEdit;

        private void AddMode(bool add)
        {
            if (add)
            {
                btnAdd.Visibility = Visibility.Visible;
                btnSave.Visibility = Visibility.Visible;
                btnSwapUp.Visibility = Visibility.Visible;
                btnSwapDown.Visibility = Visibility.Visible;
                btnDelete.Visibility = Visibility.Visible;
            }
            else
            {
                btnAdd.Visibility = Visibility.Hidden;
                btnSave.Visibility = Visibility.Hidden;
                btnSwapUp.Visibility = Visibility.Hidden;
                btnSwapDown.Visibility = Visibility.Hidden;
                btnDelete.Visibility = Visibility.Hidden;
            }
        }

        public void EditMode(bool edit)
        {
            AddMode(edit);
            _xEdit = edit;

            spFVs.IsEnabled = edit;
            btnSave.IsEnabled = false;
            if (_oForceVectors != null && _oForceVectors.Count > 0)
                DrawForceVector(_oForceVectors[cmbSelector.SelectedIndex]);
            if (_xEdit) { cmbSelector.Margin = new Thickness(10, 40, 0, 0); } else { cmbSelector.Margin = new Thickness(10, 10, 0, 0); }

        }

        private void DrawForceVector(clsForceVector oForceVector)
        {
            spFVs.Children.Clear();

            int iX = 5;
            int iY = 0;
            if (_xEdit) { iY = 65; } else { iY = 35;  }

            Grid grid = new Grid();
            ucForceVector uc = new ucForceVector(oForceVector);
            uc.EditMode(_xEdit);
            uc.Margin = new Thickness(iX, iY, 0, 0);
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.HorizontalAlignment = HorizontalAlignment.Left;
            //lstPoses.Content = grid;
            //grid.Width = uc.Width + 10;
            //grid.Height = uc.Height + 70;
            spFVs.Children.Add(grid);
              
            //spFVs.Width = grid.Width + 10;
            //spFVs.Height = grid.Height + 20;

            //gpbForceVectors.Width = spFVs.Width + 10;
            //gpbForceVectors.Height = spFVs.Height + 20;
            //View.Height = gpbPoses.Height + 10;
            //View.Width = gpbPoses.Width + 10;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox a = (ComboBox)sender;
            if (a.SelectedIndex >= 0)
                DrawForceVector(_oForceVectors[a.SelectedIndex]);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            btnAdd.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnSwapUp.IsEnabled = false;
            btnSwapDown.IsEnabled = false;
            btnDelete.IsEnabled = false;

            spFVs.Children.Clear();

            int iX = 5;
            int iY = 0;
            if (_xEdit) { iY = 65; } else { iY = 35; }

            Grid grid = new Grid();
            ucForceVector uc = new ucForceVector();

            uc.EditMode(_xEdit);
            uc.Margin = new Thickness(iX, iY, 0, 0);
            uc.VerticalAlignment = VerticalAlignment.Top;
            uc.HorizontalAlignment = HorizontalAlignment.Left;
            grid.Children.Add(uc);
            grid.Margin = new Thickness(0, 0, 0, 0);
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.HorizontalAlignment = HorizontalAlignment.Left;
            //lstPoses.Content = grid;
            //grid.Width = uc.Width + 10;
            //grid.Height = uc.Height + 70;
            spFVs.Children.Add(grid);
              
            //spFVs.Width = grid.Width + 10;
            //spFVs.Height = grid.Height + 20;

            //gpbForceVectors.Width = spFVs.Width + 10;
            //gpbForceVectors.Height = spFVs.Height + 20;
            //View.Height = gpbPoses.Height + 10;
            //View.Width = gpbPoses.Width + 10;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            btnAdd.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnSwapUp.IsEnabled = true;
            btnSwapDown.IsEnabled = true;
            btnSwapDown.IsEnabled = true;
            btnDelete.IsEnabled = true;

            Grid grid = (Grid)spFVs.Children[0];
            ucForceVector uc = (ucForceVector)grid.Children[0];
            if (_oForceVectors == null) { _oForceVectors = new List<clsForceVector>(); }


            int iIndex = cmbSelector.SelectedIndex;
            _oForceVectors.Insert(iIndex + 1, uc.getForceVector());
            List<string> oItems = new List<string>();
            for (int i = 0; i < _oForceVectors.Count; i++)
            {
                oItems.Add(i.ToString());
            }
            cmbSelector.ItemsSource = oItems;
            cmbSelector.SelectedIndex = iIndex + 1;
        }

        private void btnSwapUp_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = cmbSelector.SelectedIndex;
            if (iIndex == 0) { return; }
            var oPose = _oForceVectors[iIndex];
            _oForceVectors.RemoveAt(iIndex);
            _oForceVectors.Insert(iIndex - 1, oPose);
            cmbSelector.SelectedIndex = iIndex - 1;
        }
        private void btnSwapDown_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = cmbSelector.SelectedIndex;
            if (iIndex >= _oForceVectors.Count - 1) { return; }
            var oPose = _oForceVectors[iIndex];
            _oForceVectors.RemoveAt(iIndex);
            _oForceVectors.Insert(iIndex + 1, oPose);
            cmbSelector.SelectedIndex = iIndex + 1;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            int iIndex = cmbSelector.SelectedIndex;
            if (iIndex < 0 || iIndex > _oForceVectors.Count - 1) { return; }
            _oForceVectors.RemoveAt(iIndex);

            Grid grid = (Grid)spFVs.Children[0];
            ucForceVector uc = (ucForceVector)grid.Children[0];
            if (_oForceVectors == null) { _oForceVectors = new List<clsForceVector>(); }

            if (_oForceVectors.Count == 0) 
            {
                cmbSelector.ItemsSource = null;
                cmbSelector.SelectedIndex = -1;
                spFVs.Children.Clear();
                return; 
            }
            iIndex = cmbSelector.SelectedIndex -1;
            if (iIndex < 0) { iIndex = 0; }
            List<string> oItems = new List<string>();
            for (int i = 0; i < _oForceVectors.Count; i++)
            {
                oItems.Add(i.ToString());
            }
            cmbSelector.ItemsSource = oItems;
            cmbSelector.SelectedIndex = iIndex;

        }
    }
}
