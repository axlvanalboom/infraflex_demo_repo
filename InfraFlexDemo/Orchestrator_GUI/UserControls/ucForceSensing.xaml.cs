﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;
namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucForceSensing.xaml
    /// </summary>
    public partial class ucForceSensing : UserControl
    {
        public ucForceSensing()
        {
            InitializeComponent();
            cmbOrigin.ItemsSource = Enum.GetNames(typeof(SourceType));
            cmbOrigin.SelectedIndex = 0;
        }
        public ucForceSensing(clsForceSensing oForceSensing)
        {
            InitializeComponent();
            setForceSensing(oForceSensing);
        }

        public void setForceSensing(clsForceSensing oForceSensing)
        {
            cmbOrigin.ItemsSource = Enum.GetNames(typeof(SourceType));
            cmbOrigin.SelectedItem = oForceSensing.ForceOrigin.ToString();
            txtListeningId.Text = oForceSensing.ForceListeningId;
            if (oForceSensing.ForceListeningId == null ) { txtListeningId.Text = ""; }
            gpbForceSensing.IsEnabled = false;
        }
        public clsForceSensing getForceSensing()
        {
            SourceType stType = (SourceType)Enum.Parse(typeof(SourceType), cmbOrigin.SelectedItem.ToString());
            return new clsForceSensing(stType, txtListeningId.Text);
        }

        public void EditMode(bool edit)
        {
            gpbForceSensing.IsEnabled = edit;
        }

        public event EventHandler HeightChanged; // so everything gets drawn nice
        private void cmbOrigin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbOrigin.SelectedItem.ToString() == SourceType.Internal.ToString()) {gpbForceSensing.Height = 60;}
            else { gpbForceSensing.Height = 90; }
            EventHandler oHeigtChanged = HeightChanged;
            oHeigtChanged?.Invoke(this, e);
        }
    }
}
