﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucMoveForce.xaml
    /// </summary>
    public partial class ucMoveForce : UserControl
    {
        public ucMoveForce()
        {
            InitializeComponent();
        }
        public ucMoveForce(clsMoveForce oMoveForce)
        {
            InitializeComponent(); setMoveForce(oMoveForce);
        }
    
        public void setMoveForce(clsMoveForce oMoveForce)
        {
            clsMove oMove = new clsMove(oMoveForce.Object, oMoveForce.Type, oMoveForce.FrameworkId, oMoveForce.ToolId,oMoveForce.Homing, oMoveForce.Poses, oMoveForce.TopicId);
            MOVE.setMove(oMove);     
            FORCESENSING.setForceSensing(oMoveForce.ForceSensing);
            FORCEVECTORS.setForceVectors(oMoveForce.ForceVectors);

            EditMode(false);
        }
        public clsMoveForce getMoveForce()
        {
            var oMoveForce = new clsMoveForce();
            oMoveForce.ForceSensing = FORCESENSING.getForceSensing();
            oMoveForce.ForceVectors = FORCEVECTORS.getForceVectors();

            var oMove = MOVE.getMove();
            oMoveForce.Object = oMove.Object;
            oMoveForce.Type = oMove.Type;
            oMoveForce.FrameworkId = oMove.FrameworkId;
            oMoveForce.ToolId = oMove.ToolId;
            oMoveForce.Homing = oMove.Homing;
            oMoveForce.Poses = oMove.Poses;
            oMoveForce.TopicId = oMove.TopicId;

            return oMoveForce;
        }
    
        public void EditMode(bool edit)
        {
            MOVE.EditMode(edit);
            FORCESENSING.EditMode(edit);
            FORCEVECTORS.EditMode(edit);
        }
    
    }
}
