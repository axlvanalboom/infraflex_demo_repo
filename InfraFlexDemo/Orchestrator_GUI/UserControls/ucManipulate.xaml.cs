﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;
namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucManipulate.xaml
    /// </summary>
    public partial class ucManipulate : UserControl
    {
        public ucManipulate()
        {
            InitializeComponent();
        }
        public ucManipulate(clsManipulate oManipulate)
        {
            InitializeComponent();
            setManipulate(oManipulate);
        }
        public void setManipulate(clsManipulate oManipulate)
        {
            PART.SetPart(oManipulate.Object);
            GENERAL.setGeneral(oManipulate.General);
            gpbManipulate.IsEnabled = false;
        }
        public clsManipulate getManipulate()
        {
            return new clsManipulate(PART.getPart(), GENERAL.getGeneral());
        }

        public void EditMode(bool edit)
        {
            gpbManipulate.IsEnabled = edit;
            PART.EditMode(edit);
            GENERAL.EditMode(edit);
        }

    }
}
