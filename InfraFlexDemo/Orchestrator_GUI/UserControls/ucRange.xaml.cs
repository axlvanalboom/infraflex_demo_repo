﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucRange.xaml
    /// </summary>
    public partial class ucRange : UserControl
    {
        public ucRange()
        {
            InitializeComponent();
            setHeader("Range");
        }

        public ucRange(clsRange oRange)
        {
            InitializeComponent(); setRange(oRange);
        }

        public void setRange(clsRange oRange)
        {
            txtMin.Text = oRange.Minima.ToString();
            txtMax.Text = oRange.Maxima.ToString();
            gpbRange.IsEnabled = false;
        }
        public void setHeader(string sHeader)
        {
            gpbRange.Header = sHeader;
            EditMode(false);
        }
        public clsRange getRange()
        {
            double fMin; //= double.Parse(txtMin.Text);
            double fMax;// = double.Parse(txtMax.Text);
            double.TryParse(txtMin.Text.Replace(".", ","), out fMin);
            double.TryParse(txtMax.Text.Replace(".", ","), out fMax);
            return new clsRange(fMin, fMax);
        }

        public void EditMode(bool edit)
        {
            gpbRange.IsEnabled = edit;
        }

        private void txtMin_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtMin.Text))
            {
                txtMin.Text = "0";
            }
        }
        private void txtMax_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtMax.Text))
            {
                txtMax.Text = "0";
            }
        }
    }
}
