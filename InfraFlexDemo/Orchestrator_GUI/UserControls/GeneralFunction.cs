﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Orchestrator_GUI.UserControls
{
    public class clsGeneralUcFunctions
    {

        private static readonly Regex _oDouble = new Regex("[+-]?([0-9]*[.])?[0-9]+");//new Regex("^-?[0-9]"); //regex that matches disallowed text
        public static bool IsDouble(string text)
        {
            if (text.Length == 0)
                return true;

            var iAmountP = text.Count(x => x == 'p');
            if (iAmountP > 1) { return false; }
            if (iAmountP == 1) {
                if (text[0] != 'p') { return false; }
                else text = text.Substring(1, text.Length - 1);
            }

            var iAmountR = text.Count(x => x == 'r');
            if (iAmountR > 1) { return false; }
            if (iAmountR == 1) {
                if (text[0] != 'r') { return false; }
                else text = text.Substring(1, text.Length - 1);
            }

            var iAmountMin = text.Count(x => x == '-');
            if (iAmountMin > 1) { return false; }
            if (iAmountMin == 1) { if (text[0] != '-') { return false; } }

            var iAmountDot = text.Count(x => x == '.');

            if (iAmountDot > 1) { return false; }

            var iAmountComma = text.Count(x => x == ',');
            if (iAmountComma > 1) { return false; }

            if (iAmountDot + iAmountComma > 1) { return false; }

            return text.Length == 0 || text == "-" || _oDouble.IsMatch(text);


        }

        private static readonly Regex _oInt = new Regex("[0-9]"); //regex that matches disallowed text
        public static bool IsInt(string text)
        {

            return text.Length == 0 || _oInt.IsMatch(text);
        }

    }
}
