﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BEC;

namespace Orchestrator_GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ucPart.xaml
    /// </summary>
    public partial class ucPart : UserControl
    {
        public ucPart()
        {
            InitializeComponent();
        }

        public ucPart(clsPart oPart)
        {
            InitializeComponent();
            SetPart(oPart);
        }
        
        public void SetPart(clsPart oPart)
        {
            txtMass.Text = oPart.Mass == 0 ? "" : oPart.Mass.ToString();
            txtX.Text = double.Parse(oPart.CenterOfGravity.X) == 0 ? "" : oPart.CenterOfGravity.X.ToString();
            txtY.Text = double.Parse(oPart.CenterOfGravity.Y) == 0 ? "" : oPart.CenterOfGravity.Y.ToString();
            txtZ.Text = double.Parse(oPart.CenterOfGravity.Z) == 0 ? "" : oPart.CenterOfGravity.Z.ToString();

            gpbPart.IsEnabled = false;
        }

        public void EditMode(bool edit)
        {
            gpbPart.IsEnabled = edit;
        }

        public clsPart getPart()
        {
            try
            {
                double fM;// = double.Parse(txtMass.Text);
                double fX;// = double.Parse(txtX.Text);
                double fY;// = double.Parse(txtY.Text);
                double fZ;// = double.Parse(txtZ.Text);

                double.TryParse(txtMass.Text.Replace(".",","), out fM);
                double.TryParse(txtX.Text.Replace(".",","), out fX);
                double.TryParse(txtY.Text.Replace(".",","), out fY);
                double.TryParse(txtZ.Text.Replace(".", ","), out fZ);

                return new clsPart(new clsPosition(fX.ToString(), fY.ToString(), fZ.ToString()),fM);
            }
            catch (Exception e) { throw new Exception("An input is wrong!"); }
        }

        private void txtMass_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtMass.Text))
            {
                txtMass.Text = "";
            }
        }
        private void txtX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtX.Text))
            {
                txtX.Text = "";
            }
        }
        private void txtY_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtY.Text))
            {
                txtY.Text = "";
            }
        }
        private void txtZ_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!clsGeneralUcFunctions.IsDouble(txtZ.Text))
            {
                txtZ.Text = "";
            }
        }
    }
}
