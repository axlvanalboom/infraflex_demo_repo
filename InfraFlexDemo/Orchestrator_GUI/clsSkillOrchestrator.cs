﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BEC;
using DataStructs;
using Newtonsoft.Json;
using DDS;

namespace Orchestrator
{
    public class clsSkillOrchestrator : IOrchestratorInterface
    {
        // Event to notify system state
        public event StatusEventHandler State;
        public event ExecutionDoneHandler Done;
        public event ExecutionStepHandler Step;
        public event RecipeChangeHandler ChangeRecipe;

        // Vars
        private List<clsDetectedObject> _lstRespons;
        private List<clsRecipe> _lstRecipes;
        private bool _xAutoLoop = false;
        private List<string> _lstSkillTypes;
        private int _iCurrentRecipe;
        private int _iCurrentStep;
        private int _iResourceId;
        private bool _xRun;
        private clsInfluxDbFunctions _oInflux;
        private int _iCount = 0;
        private Dictionary<string, DateTime> _oTimers;

        private clsDDSParticipant _oParticipant;
        StatusDataReader _oReader;
        ExecutionDataWriter _oWriter;
        private Tuple<Status, DateTime> _oState;

        // Locking object
        private readonly object _oLock = new object();

        public clsSkillOrchestrator(int iResourceId = 1, string sStatusTopic = "Status", string sExecutionTopic = "Execution")
        {
            lock (_oLock)
            {
                // Initiate vars
                _xRun = false;
                _iCurrentStep = 0;
                _iCurrentRecipe = 0;
                _iResourceId = iResourceId;
                _lstRecipes = new List<clsRecipe>();

                _lstSkillTypes = new List<string>();
                _lstSkillTypes.Add("Move");
                _lstSkillTypes.Add("MoveForce");
                _lstSkillTypes.Add("Manipulate");
                _lstSkillTypes.Add("Detect");
                _lstSkillTypes.Add("Timer");

                _oState = new Tuple<Status, DateTime>(new Status(), DateTime.MinValue);

                // Create participant
                _oParticipant = new clsDDSParticipant("Orchestrator", Orchestrator_GUI.Properties.Settings.Default.QoS_File, Orchestrator_GUI.Properties.Settings.Default.QoS_Profile);

                // Create delegate for read function
                clsListener oListener = new clsListener();
                oListener.OnDataAvailableHandler += OListener_DataAvailableHandler;

                // Add datareader
                _oReader = _oParticipant.addReader(sStatusTopic, new StatusTypeSupport(), oListener) as StatusDataReader;

                // Add datawriter
                _oWriter = _oParticipant.addWriter(sExecutionTopic, new ExecutionTypeSupport()) as ExecutionDataWriter;

                // Initialise Influx DB client
                _oInflux = new clsInfluxDbFunctions("http://localhost:8086", "cpp");

                // Initiate timers
                _oTimers = new Dictionary<string, DateTime>();
            }
        }

        public bool Running() 
        {
            return _xRun;
        }

        private void OListener_DataAvailableHandler(IDataReader oReader)
        {
            lock (_oLock)
            {
                // Lock previous state
                string sPreviousState = _oState.Item1.State;

                // New state received
                Status[] arrStates = null;
                SampleInfo[] arrInfo = null;

                // Take the new message
                _oReader.Take(ref arrStates, ref arrInfo);

                // Only look at the latest state
                if (arrStates.Length <=0) { return; }

                _oState = new Tuple<Status, DateTime>(arrStates[arrStates.Length - 1], DateTime.Now);

                // Launch event with new status to GUI
                State?.Invoke(_oState);

                // Handle state transitions (init, idle, busy, error)
                clsRespons oRespons = JsonConvert.DeserializeObject<clsRespons>(_oState.Item1.State);

                if (oRespons.Info != null) 
                {
                    _lstRespons = oRespons.Info.DetectedObjects;
                }

                switch (oRespons.State)
                {
                    case ("Init"):

                        break;
                    case ("Idle"):
                        if (sPreviousState.Contains("Busy"))
                            _iCurrentStep++;
                        sendExecution();
                        break;
                    case ("Busy"):
                        break;
                    case ("Error"):

                        break;
                    default:

                        break;
                }
            }
        }

        // Write current execution step
        private void sendExecution()
        {
            if (_xRun)
            {
                // Check timer functions first!!
                while (_iCurrentStep < _lstRecipes[_iCurrentRecipe].Skills.Count && _lstRecipes[_iCurrentRecipe].Skills[_iCurrentStep].GetType() == typeof(clsTimer))
                {
                    clsTimer oTimer = (clsTimer)_lstRecipes[_iCurrentRecipe].Skills[_iCurrentStep];
                    if (oTimer.StartStop)
                    {
                        // start timer(s)
                        _oTimers.Add(oTimer.Tag, DateTime.UtcNow);
                    }
                    else
                    {
                        // Stop the timer(s) and log the time with the tag
                        DateTime oStart;
                        _oTimers.TryGetValue(oTimer.Tag, out oStart);
                        if (oStart != null)
                        {
                            _oTimers.Remove(oTimer.Tag);
                            DateTime oStop = DateTime.UtcNow;
                            TimeSpan oSpan = oStop.Subtract(oStart);
                            // logging here
                            _oInflux.LogData(new clsLog(oTimer.Tag,_iCurrentRecipe, oSpan.TotalSeconds));
                            Console.WriteLine($"Tag: {oTimer.Tag}, Time: {oSpan.TotalMilliseconds}");
                        }
                    }
                    _iCurrentStep++;
                }

                // then perform the regular executions.
                if (_iCurrentStep < _lstRecipes[_iCurrentRecipe].Skills.Count)
                {
                    string sCurrentSkill = JsonConvert.SerializeObject(_lstRecipes[_iCurrentRecipe].Skills[_iCurrentStep], new clsSkillParametersJsonConverter());
                    clsSkillParameter oCurrentSkill = JsonConvert.DeserializeObject<clsSkillParameter>(sCurrentSkill, new clsSkillParametersJsonConverter());

                    // Write execution to topic
                    Execution oExec = new Execution();
                    oExec.ResourceId = oCurrentSkill.ResourceId;
                    oCurrentSkill.Counter = _iCount;

                    // Check current skill for parameters / randomize
                    if (oCurrentSkill.GetType() == typeof(clsMove)) 
                    {
                        clsMove oMove = (clsMove)oCurrentSkill;
                        foreach (clsPose oPose in oMove.Poses)
                        {
                            if (oPose.Position.R.Contains("p"))
                            {
                                int iParameter = -1;
                                Int32.TryParse(oPose.Position.R.Substring(1, oPose.Position.R.Length - 1), out iParameter);
                                if (iParameter >= 0 && _lstRespons != null && _lstRespons.Count > iParameter)
                                    oPose.Position.R = _lstRespons[iParameter].Angle.ToString("N10").Replace(',', '.');
                            }

                            if (oPose.Position.R.Contains("r"))
                            {
                                int iMax = -1;
                                Int32.TryParse(oPose.Position.R.Substring(1, oPose.Position.R.Length - 1), out iMax);
                                if (iMax >= 0)
                                {
                                    Random random = new Random();
                                    oPose.Position.R = ((random.NextDouble() * iMax * 2) - iMax).ToString("N10").Replace(',', '.');
                                }
                            }
                        }
                    }

                    oExec.Parameter = JsonConvert.SerializeObject(oCurrentSkill);
                    _oWriter.Write(oExec);

                    // New execution step
                    Step?.Invoke(new Tuple<Execution, DateTime>(oExec, DateTime.Now),_iCurrentStep) ;
                }
                else
                {
                    // Check if autoloop is active
                    if (_xAutoLoop) 
                    {
                        // Read recipe to perform from DB
                        //_iCurrentRecipe = _oInflux.ReadCurrentRecipe();
                        if (_iCurrentRecipe == 0)
                            _iCurrentRecipe = 1;
                        else
                            _iCurrentRecipe = 0;

                        // Load recipe in GUI
                        ChangeRecipe?.Invoke(_iCurrentRecipe, _lstRecipes[_iCurrentRecipe].Skills);

                        // Start new execution
                        _iCurrentStep = 0;
                        sendExecution();
                    }
                    else 
                    {
                        // Execution is done
                        Done?.Invoke();
                    }
                }

                _iCount++;
            }
        }

        public bool Start(int iStartAtIndex = 0)
        {
            lock (_oLock)
            {

                if (_lstRecipes[_iCurrentRecipe].Skills.Count > 0 && iStartAtIndex > -1 && iStartAtIndex < _lstRecipes[_iCurrentRecipe].Skills.Count)
                {
                    _xRun = true;
                    _iCurrentStep = iStartAtIndex;

                    // Launch first execution
                    sendExecution();

                    return true;
                }
                else
                    return false;
            }
        }
        public bool Resume()
        {
            return Start(_iCurrentStep);
        }

        public bool Pause()
        {
            return Stop(false);
        }

        public bool Stop(bool xReturnToFirstStep = false)
        {
            lock (_oLock)
            {
                _xRun = false;
                if (xReturnToFirstStep)
                {
                    _iCurrentStep = 0;
                    _oTimers = new Dictionary<string, DateTime>();
                }

                return true;
            }
        }

        public void addRecipe()
        {
            _lstRecipes.Add(new clsRecipe());
            _iCurrentRecipe = _lstRecipes.Count - 1;
        }

        public int RecipeId() 
        {
            return _iCurrentRecipe;
        }

        public void deleteRecipe(int iIndex) 
        {
            if (iIndex < _lstRecipes.Count)
                _lstRecipes.RemoveAt(iIndex);
            if (_iCurrentRecipe > _lstRecipes.Count - 1)
                _iCurrentRecipe = 0;
        }

        public List<string> getRecipes() 
        {
            List<string> lstReturn = new List<string>();

            foreach (clsRecipe oRecipe in _lstRecipes)
            {
                if (oRecipe.Name != null && oRecipe.Name != "")
                    lstReturn.Add(oRecipe.Name);
                else
                    lstReturn.Add("Empty name");
            }

            return lstReturn;
        }

        public void setRecipe(int iRecipe) 
        {
            if (iRecipe < _lstRecipes.Count)
                _iCurrentRecipe = iRecipe;
        }

        public void editRecipeName(string sName) 
        {
            _lstRecipes[_iCurrentRecipe].Name = sName;
        }

        public string getRecipeName()
        {
            return _lstRecipes[_iCurrentRecipe].Name;
        }

        public bool addSkill(clsSkillParameter oSkillParameter, int iInsertAtIndex = -1)
        {
            lock (_oLock)
            {
                if (iInsertAtIndex > -1 && iInsertAtIndex < _lstRecipes[_iCurrentRecipe].Skills.Count)
                    _lstRecipes[_iCurrentRecipe].Skills.Insert(iInsertAtIndex, oSkillParameter);
                else
                    _lstRecipes[_iCurrentRecipe].Skills.Add(oSkillParameter);
                return true;
            }
        }

        public bool deleteSkill(int iDeleteAtIndex)
        {
            lock (_oLock)
            {
                if (iDeleteAtIndex > -1 && iDeleteAtIndex < _lstRecipes[_iCurrentRecipe].Skills.Count)
                {
                    _lstRecipes[_iCurrentRecipe].Skills.RemoveAt(iDeleteAtIndex);
                    return true;
                }
                return false;
            }
        }

        public bool editSkill(clsSkillParameter oSkillParameter, int iEditAtIndex)
        {
            lock (_oLock)
            {
                if (iEditAtIndex > -1 && iEditAtIndex < _lstRecipes[_iCurrentRecipe].Skills.Count)
                {
                    _lstRecipes[_iCurrentRecipe].Skills[iEditAtIndex] = oSkillParameter;
                    return true;
                }
                return false;
            }
        }

        public bool swapSkillParameters(int iIndex1, int iIndex2)
        {
            lock (_oLock)
            {
                if (iIndex1 > -1 && iIndex1 < _lstRecipes[_iCurrentRecipe].Skills.Count && iIndex2 > -1 && iIndex2 < _lstRecipes[_iCurrentRecipe].Skills.Count)
                {
                    clsSkillParameter oParams = _lstRecipes[_iCurrentRecipe].Skills[iIndex2];
                    _lstRecipes[_iCurrentRecipe].Skills[iIndex2] = _lstRecipes[_iCurrentRecipe].Skills[iIndex1];
                    _lstRecipes[_iCurrentRecipe].Skills[iIndex1] = oParams;
                    return true;
                }
                else
                    return false;
            }
        }

        public clsSkillParameter getSkillParameter(int iIndex)
        {
            lock (_oLock)
            {
                if (iIndex > -1 && iIndex < _lstRecipes[_iCurrentRecipe].Skills.Count)
                    return _lstRecipes[_iCurrentRecipe].Skills[iIndex];
                else
                    return null;
            }
        }

        public List<string> getSkillTypes()
        {
            lock (_oLock)
            {
                return _lstSkillTypes;
            }
        }

        public List<clsSkillParameter> getSkills()
        {  
            lock (_oLock)
            {
                if (_lstRecipes != null && _iCurrentRecipe < _lstRecipes.Count)
                    return _lstRecipes[_iCurrentRecipe].Skills;
                else
                    return new List<clsSkillParameter>();
            }
        }

        public string saveListToFile(string sFileName = "SkillParameters.txt")
        {
            lock (_oLock)
            {
                try
                {
                    string sContent = JsonConvert.SerializeObject(_lstRecipes[_iCurrentRecipe].Skills, new clsSkillParametersJsonConverter());
                    File.WriteAllText(sFileName, sContent);
                    return $"Parameters succesfully saved in {sFileName}.";
                }
                catch (Exception oEx)
                {
                    return oEx.Message;
                }
            }
        }

        public string loadListFromFile(string sFileName = "SkillParameters.txt")
        {
            lock (_oLock)
            {
                try
                {
                    string sContent = File.ReadAllText(sFileName);
                    var o = JsonConvert.DeserializeObject<List<clsSkillParameter>>(sContent);

                    _lstRecipes[_iCurrentRecipe].Skills = JsonConvert.DeserializeObject<List<clsSkillParameter>>(sContent, new clsSkillParametersJsonConverter());
                    return $"Parameters succesfully loaded from {sFileName}.";
                }
                catch (Exception oEx)
                {
                    return oEx.Message;
                }
            }
        }

        public string saveRecipesToFile(string sFileName = "Recipes.txt")
        {
            lock (_oLock)
            {
                try
                {
                    string sContent = JsonConvert.SerializeObject(_lstRecipes, new clsSkillParametersJsonConverter());
                    File.WriteAllText(sFileName, sContent);
                    return $"Recipes succesfully saved in {sFileName}.";
                }
                catch (Exception oEx)
                {
                    return oEx.Message;
                }
            }
        }

        public string loadRecipesFromFile(string sFileName = "Recipes.txt")
        {
            lock (_oLock)
            {
                try
                {
                    string sContent = File.ReadAllText(sFileName);
                    var o = JsonConvert.DeserializeObject<List<clsRecipe>>(sContent);

                    _lstRecipes = JsonConvert.DeserializeObject<List<clsRecipe>>(sContent, new clsRecipeJsonConverter());
                    return $"Recipes succesfully loaded from {sFileName}.";
                }
                catch (Exception oEx)
                {
                    return oEx.Message;
                }
            }
        }

        public void destroy()
        {
            _oParticipant.destroy();
        }

        public void StartAutoloop() 
        {
            _xAutoLoop = true;
        }

        public void StopAutoloop()
        {
            _xAutoLoop = false;
        }
    }
}
