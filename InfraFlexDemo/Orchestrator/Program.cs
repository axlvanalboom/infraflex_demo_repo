﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDS;
using BEC;
using DataStructs;

namespace Orchestrator
{
    class Program
    {
        static void Main(string[] args)
        {
           
        }

        private static void OOrch_Done()
        {
            Console.WriteLine("Execution done");
        }

        private static void OOrch_State(Tuple<Status, DateTime> oStateAndUpdateTime)
        {
            Console.WriteLine($"New state received '${oStateAndUpdateTime.Item1.State}:${oStateAndUpdateTime.Item1.Info}' at ${oStateAndUpdateTime.Item2.ToLocalTime().ToString()}");
        }
    }
}
