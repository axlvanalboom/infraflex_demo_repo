﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BEC;
using DataStructs;
using Newtonsoft.Json;
using DDS;

namespace Orchestrator
{
    public class clsSkillOrchestrator : IOrchestratorInterface
    {
        // Event to notify system state
        public event StatusEventHandler State;
        public event ExecutionDoneHandler Done;
        public event ExecutionStepHandler Step;

        // Vars
        private List<clsSkillParameter> _lstSkillParameters;
        private List<string> _lstSkillTypes;
        private int _iCurrentStep;
        private int _iResourceId;
        private bool _xRun;

        private clsDDSParticipant _oParticipant;
        StatusDataReader _oReader;
        ExecutionDataWriter _oWriter;
        private Tuple<Status, DateTime> _oState;

        // Locking object
        private readonly object _oLock = new object();

        public clsSkillOrchestrator(int iResourceId = 1, string sStatusTopic = "Status", string sExecutionTopic = "Execution")
        {
            lock (_oLock)
            {
                // Initiate vars
                _xRun = false;
                _iCurrentStep = 0;
                _iResourceId = iResourceId;
                _lstSkillParameters = new List<clsSkillParameter>();

                _lstSkillTypes = new List<string>();
                _lstSkillTypes.Add("Move");
                _lstSkillTypes.Add("MoveForce");
                _lstSkillTypes.Add("Manipulate");

                _oState = new Tuple<Status, DateTime>(new Status(), DateTime.MinValue);

                // Create participant
                _oParticipant = new clsDDSParticipant("Orchestrator", Properties.Settings.Default.QoS_File, Properties.Settings.Default.QoS_Profile);

                // Create delegate for read function
                clsListener oListener = new clsListener();
                oListener.OnDataAvailableHandler += OListener_DataAvailableHandler;

                // Add datareader
                _oReader = _oParticipant.addReader(sStatusTopic, new StatusTypeSupport(), oListener) as StatusDataReader;

                // Add datawriter
                _oWriter = _oParticipant.addWriter(sExecutionTopic, new ExecutionTypeSupport()) as ExecutionDataWriter;
            }
        }

        private void OListener_DataAvailableHandler(IDataReader oReader)
        {
            lock (_oLock)
            {
                // Lock previous state
                string sPreviousState = _oState.Item1.State;

                // New state received
                Status[] arrStates = null;
                SampleInfo[] arrInfo = null;

                // Take the new message
                _oReader.Take(ref arrStates, ref arrInfo);

                // Only look at the latest state
                _oState = new Tuple<Status, DateTime>(arrStates[arrStates.Length - 1], DateTime.Now);

                // Launch event with new status to GUI
                State?.Invoke(_oState);

                // Handle state transitions (init, idle, busy, error)
                switch (_oState.Item1.State) 
                {
                    case ("init"):

                        break;
                    case ("idle"):
                        if (sPreviousState == "busy")
                            _iCurrentStep++;
                        sendExecution();
                        break;
                    case ("busy"):

                        break;
                    case ("error"):

                        break;
                    default:

                        break;
                }
            }
        }

        // Write current execution step
        private void sendExecution() 
        {
            if (_xRun)
            {
                if (_iCurrentStep < _lstSkillParameters.Count)
                {
                    // Write execution to topic
                    Execution oExec = new Execution();
                    oExec.ResourceId = _iResourceId;
                    oExec.Parameter = JsonConvert.SerializeObject(_lstSkillParameters[_iCurrentStep]);
                    _oWriter.Write(oExec);

                    // New execution step
                    Step?.Invoke(new Tuple<Execution, DateTime>(oExec, DateTime.Now));
                }
                else
                {
                    // Execution is done
                    Done?.Invoke();
                }
            }
        }

        public bool Start(int iStartAtIndex = 0) 
        {
            lock (_oLock)
            {

                if (_lstSkillParameters.Count > 0 && iStartAtIndex > -1 && iStartAtIndex < _lstSkillParameters.Count)
                {
                    _xRun = true;
                    _iCurrentStep = iStartAtIndex;

                    // Launch first execution
                    sendExecution();

                    return true;
                }
                else
                    return false;
            }
        }
        public bool Resume()
        {
            return Start(_iCurrentStep);
        }

        public bool Pause() 
        {
            return Stop(false);
        }

        public bool Stop(bool xReturnToFirstStep = false) 
        {
            lock (_oLock)
            {
                _xRun = false;
                if (xReturnToFirstStep)
                    _iCurrentStep = 0;

                return true;
            }
        }

        public bool addSkill(clsSkillParameter oSkillParameter, int iInsertAtIndex = -1)
        {
            lock (_oLock)
            {
                if (iInsertAtIndex > -1 && iInsertAtIndex < _lstSkillParameters.Count)
                    _lstSkillParameters.Insert(iInsertAtIndex, oSkillParameter);
                else
                    _lstSkillParameters.Add(oSkillParameter);
                return true;
            }
        }

        public bool deleteSkill(int iDeleteAtIndex)
        {
            lock (_oLock)
            {
                if (iDeleteAtIndex > -1 && iDeleteAtIndex < _lstSkillParameters.Count)
                {
                    _lstSkillParameters.RemoveAt(iDeleteAtIndex);
                    return true;
                }
                return false;
            }
        }

        public bool editSkill(clsSkillParameter oSkillParameter, int iEditAtIndex)
        {
            lock (_oLock)
            {
                if (iEditAtIndex > -1 && iEditAtIndex < _lstSkillParameters.Count)
                {
                    _lstSkillParameters[iEditAtIndex] = oSkillParameter;
                    return true;
                }
                return false;
            }
        }

        public bool swapSkillParameters(int iIndex1, int iIndex2)
        {
            lock (_oLock)
            {
                if (iIndex1 > -1 && iIndex1 < _lstSkillParameters.Count && iIndex2 > -1 && iIndex2 < _lstSkillParameters.Count)
                {
                    clsSkillParameter oParams = _lstSkillParameters[iIndex2];
                    _lstSkillParameters[iIndex2] = _lstSkillParameters[iIndex1];
                    _lstSkillParameters[iIndex1] = oParams;
                    return true;
                }
                else
                    return false;
            }
        }
        
        public clsSkillParameter getSkillParameter(int iIndex)
        {
            lock (_oLock)
            {
                if (iIndex > -1 && iIndex < _lstSkillParameters.Count)
                    return _lstSkillParameters[iIndex];
                else
                    return null;
            }
        }

        public List<string> getSkillTypes()
        {
            lock (_oLock)
            {
                return _lstSkillTypes;
            }
        }

        public List<clsSkillParameter> getSkills()
        {
            lock (_oLock)
            {
                return _lstSkillParameters;
            }
        }

        public string saveListToFile(string sFileName = "SkillParameters.txt")
        {
            lock (_oLock)
            {
                try
                {
                    string sContent = JsonConvert.SerializeObject(_lstSkillParameters, new clsSkillParametersJsonConverter());
                    File.WriteAllText(sFileName, sContent);
                    return $"Parameters succesfully saved in {sFileName}.";
                }
                catch (Exception oEx)
                {
                    return oEx.Message;
                }
            }
        }

        public string loadListFromFile(string sFileName = "SkillParameters.txt")
        {
            lock (_oLock)
            {
                try
                {
                    string sContent = File.ReadAllText(sFileName);
                    var o = JsonConvert.DeserializeObject<List<clsSkillParameter>>(sContent);

                    _lstSkillParameters = JsonConvert.DeserializeObject<List<clsSkillParameter>>(sContent, new clsSkillParametersJsonConverter());
                    return $"Parameters succesfully loaded from {sFileName}.";
                }
                catch (Exception oEx)
                {
                    return oEx.Message;
                }
            }
        }

        public void destroy() 
        {
            _oParticipant.destroy();
        }
    }
}
