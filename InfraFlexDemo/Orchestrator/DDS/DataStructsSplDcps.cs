using DDS;
using DDS.OpenSplice.CustomMarshalers;
using DDS.OpenSplice.Database;
using DDS.OpenSplice.Kernel;
using System;
using System.Runtime.InteropServices;

namespace DataStructs
{
    #region __Status
    [StructLayout(LayoutKind.Sequential)]
    public struct __Status
    {
        public int ResourceId;
        public IntPtr State;
        public IntPtr Info;
    }
    #endregion

    #region __StatusMarshaler
    public sealed class __StatusMarshaler : DDS.OpenSplice.CustomMarshalers.FooDatabaseMarshaler<DataStructs.Status>
    {
        public static readonly string fullyScopedName = "DataStructs::Status";

        public override void InitEmbeddedMarshalers(IDomainParticipant participant)
        {
        }

        public override V_COPYIN_RESULT CopyIn(System.IntPtr typePtr, System.IntPtr from, System.IntPtr to)
        {
            GCHandle tmpGCHandle = GCHandle.FromIntPtr(from);
            DataStructs.Status fromData = tmpGCHandle.Target as DataStructs.Status;
            return CopyIn(typePtr, fromData, to);
        }

        public V_COPYIN_RESULT CopyIn(System.IntPtr typePtr, DataStructs.Status from, System.IntPtr to)
        {
            __Status nativeImg = new __Status();
            V_COPYIN_RESULT result = CopyIn(typePtr, from, ref nativeImg);
            if (result == V_COPYIN_RESULT.OK)
            {
                Marshal.StructureToPtr(nativeImg, to, false);
            }
            return result;
        }

        public V_COPYIN_RESULT CopyIn(System.IntPtr typePtr, DataStructs.Status from, ref __Status to)
        {
            if (from == null) return V_COPYIN_RESULT.INVALID;
            to.ResourceId = from.ResourceId;
            if (from.State == null) return V_COPYIN_RESULT.INVALID;
            // Unbounded string: bounds check not required...
            if (!Write(c.getBase(typePtr), ref to.State, from.State)) return V_COPYIN_RESULT.OUT_OF_MEMORY;
            if (from.Info == null) return V_COPYIN_RESULT.INVALID;
            // Unbounded string: bounds check not required...
            if (!Write(c.getBase(typePtr), ref to.Info, from.Info)) return V_COPYIN_RESULT.OUT_OF_MEMORY;
            return V_COPYIN_RESULT.OK;
        }

        public override void CopyOut(System.IntPtr from, System.IntPtr to)
        {
            __Status nativeImg = (__Status) Marshal.PtrToStructure(from, typeof(__Status));
            GCHandle tmpGCHandleTo = GCHandle.FromIntPtr(to);
            DataStructs.Status toObj = tmpGCHandleTo.Target as DataStructs.Status;
            CopyOut(ref nativeImg, ref toObj);
            tmpGCHandleTo.Target = toObj;
        }

        public override void CopyOut(System.IntPtr from, ref DataStructs.Status to)
        {
            __Status nativeImg = (__Status) Marshal.PtrToStructure(from, typeof(__Status));
            CopyOut(ref nativeImg, ref to);
        }

        public static void StaticCopyOut(System.IntPtr from, ref DataStructs.Status to)
        {
            __Status nativeImg = (__Status) Marshal.PtrToStructure(from, typeof(__Status));
            CopyOut(ref nativeImg, ref to);
        }

        public static void CopyOut(ref __Status from, ref DataStructs.Status to)
        {
            if (to == null) {
                to = new DataStructs.Status();
            }
            to.ResourceId = from.ResourceId;
            to.State = ReadString(from.State);
            to.Info = ReadString(from.Info);
        }

    }
    #endregion

    #region __Execution
    [StructLayout(LayoutKind.Sequential)]
    public struct __Execution
    {
        public int ResourceId;
        public IntPtr Parameter;
    }
    #endregion

    #region __ExecutionMarshaler
    public sealed class __ExecutionMarshaler : DDS.OpenSplice.CustomMarshalers.FooDatabaseMarshaler<DataStructs.Execution>
    {
        public static readonly string fullyScopedName = "DataStructs::Execution";

        public override void InitEmbeddedMarshalers(IDomainParticipant participant)
        {
        }

        public override V_COPYIN_RESULT CopyIn(System.IntPtr typePtr, System.IntPtr from, System.IntPtr to)
        {
            GCHandle tmpGCHandle = GCHandle.FromIntPtr(from);
            DataStructs.Execution fromData = tmpGCHandle.Target as DataStructs.Execution;
            return CopyIn(typePtr, fromData, to);
        }

        public V_COPYIN_RESULT CopyIn(System.IntPtr typePtr, DataStructs.Execution from, System.IntPtr to)
        {
            __Execution nativeImg = new __Execution();
            V_COPYIN_RESULT result = CopyIn(typePtr, from, ref nativeImg);
            if (result == V_COPYIN_RESULT.OK)
            {
                Marshal.StructureToPtr(nativeImg, to, false);
            }
            return result;
        }

        public V_COPYIN_RESULT CopyIn(System.IntPtr typePtr, DataStructs.Execution from, ref __Execution to)
        {
            if (from == null) return V_COPYIN_RESULT.INVALID;
            to.ResourceId = from.ResourceId;
            if (from.Parameter == null) return V_COPYIN_RESULT.INVALID;
            // Unbounded string: bounds check not required...
            if (!Write(c.getBase(typePtr), ref to.Parameter, from.Parameter)) return V_COPYIN_RESULT.OUT_OF_MEMORY;
            return V_COPYIN_RESULT.OK;
        }

        public override void CopyOut(System.IntPtr from, System.IntPtr to)
        {
            __Execution nativeImg = (__Execution) Marshal.PtrToStructure(from, typeof(__Execution));
            GCHandle tmpGCHandleTo = GCHandle.FromIntPtr(to);
            DataStructs.Execution toObj = tmpGCHandleTo.Target as DataStructs.Execution;
            CopyOut(ref nativeImg, ref toObj);
            tmpGCHandleTo.Target = toObj;
        }

        public override void CopyOut(System.IntPtr from, ref DataStructs.Execution to)
        {
            __Execution nativeImg = (__Execution) Marshal.PtrToStructure(from, typeof(__Execution));
            CopyOut(ref nativeImg, ref to);
        }

        public static void StaticCopyOut(System.IntPtr from, ref DataStructs.Execution to)
        {
            __Execution nativeImg = (__Execution) Marshal.PtrToStructure(from, typeof(__Execution));
            CopyOut(ref nativeImg, ref to);
        }

        public static void CopyOut(ref __Execution from, ref DataStructs.Execution to)
        {
            if (to == null) {
                to = new DataStructs.Execution();
            }
            to.ResourceId = from.ResourceId;
            to.Parameter = ReadString(from.Parameter);
        }

    }
    #endregion

}

