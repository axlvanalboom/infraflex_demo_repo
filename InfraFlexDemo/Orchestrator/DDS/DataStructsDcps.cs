using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using DDS;
using DDS.OpenSplice;
using DDS.OpenSplice.CustomMarshalers;

namespace DataStructs
{
    #region StatusDataReader
    public class StatusDataReader : DDS.OpenSplice.FooDataReader<Status, __StatusMarshaler>, 
                                         IStatusDataReader
    {
        public StatusDataReader(DatabaseMarshaler marshaler)
            : base(marshaler) { }

        public ReturnCode Read(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos)
        {
            return Read(ref dataValues, ref sampleInfos, Length.Unlimited);
        }

        public ReturnCode Read(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples)
        {
            return Read(ref dataValues, ref sampleInfos, maxSamples, SampleStateKind.Any,
                ViewStateKind.Any, InstanceStateKind.Any);
        }

        public ReturnCode Read(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            SampleStateKind sampleStates,
            ViewStateKind viewStates,
            InstanceStateKind instanceStates)
        {
            return Read(ref dataValues, ref sampleInfos, Length.Unlimited, sampleStates,
                viewStates, instanceStates);
        }

        public override ReturnCode Read(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.Read(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode Take(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos)
        {
            return Take(ref dataValues, ref sampleInfos, Length.Unlimited);
        }

        public ReturnCode Take(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples)
        {
            return Take(ref dataValues, ref sampleInfos, maxSamples, SampleStateKind.Any,
                ViewStateKind.Any, InstanceStateKind.Any);
        }

        public ReturnCode Take(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            SampleStateKind sampleStates,
            ViewStateKind viewStates,
            InstanceStateKind instanceStates)
        {
            return Take(ref dataValues, ref sampleInfos, Length.Unlimited, sampleStates,
                viewStates, instanceStates);
        }

        public override ReturnCode Take(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.Take(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode ReadWithCondition(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            IReadCondition readCondition)
        {
            return ReadWithCondition(ref dataValues, ref sampleInfos,
                Length.Unlimited, readCondition);
        }

        public override ReturnCode ReadWithCondition(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.ReadWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        readCondition);
            return result;
        }

        public ReturnCode TakeWithCondition(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            IReadCondition readCondition)
        {
            return TakeWithCondition(ref dataValues, ref sampleInfos,
                Length.Unlimited, readCondition);
        }

        public override ReturnCode TakeWithCondition(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.TakeWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        readCondition);
            return result;
        }

        public override ReturnCode ReadNextSample(
                ref Status dataValue,
                ref SampleInfo sampleInfo)
        {
            ReturnCode result =
                base.ReadNextSample(
                        ref dataValue,
                        ref sampleInfo);
            return result;
        }

        public override ReturnCode TakeNextSample(
                ref Status dataValue,
                ref SampleInfo sampleInfo)
        {
            ReturnCode result =
                base.TakeNextSample(
                        ref dataValue,
                        ref sampleInfo);
            return result;
        }

        public ReturnCode ReadInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return ReadInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode ReadInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return ReadInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode ReadInstance(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.ReadInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode TakeInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return TakeInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode TakeInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return TakeInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode TakeInstance(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.TakeInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode ReadNextInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return ReadNextInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode ReadNextInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return ReadNextInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode ReadNextInstance(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.ReadNextInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode TakeNextInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return TakeNextInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode TakeNextInstance(
            ref Status[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return TakeNextInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode TakeNextInstance(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.TakeNextInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode ReadNextInstanceWithCondition(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            return ReadNextInstanceWithCondition(
                ref dataValues,
                ref sampleInfos,
                Length.Unlimited,
                instanceHandle,
                readCondition);
        }

        public override ReturnCode ReadNextInstanceWithCondition(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.ReadNextInstanceWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        readCondition);
            return result;
        }

        public ReturnCode TakeNextInstanceWithCondition(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            return TakeNextInstanceWithCondition(
                ref dataValues,
                ref sampleInfos,
                Length.Unlimited,
                instanceHandle,
                readCondition);
        }

        public override ReturnCode TakeNextInstanceWithCondition(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.TakeNextInstanceWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        readCondition);

            return result;
        }

        public override ReturnCode ReturnLoan(
                ref Status[] dataValues,
                ref SampleInfo[] sampleInfos)
        {
            ReturnCode result =
                base.ReturnLoan(
                        ref dataValues,
                        ref sampleInfos);

            return result;
        }

        public override ReturnCode GetKeyValue(
                ref Status key,
                InstanceHandle handle)
        {
            ReturnCode result = base.GetKeyValue(
                        ref key,
                        handle);
            return result;
        }

        public override InstanceHandle LookupInstance(
                Status instance)
        {
            return
                base.LookupInstance(
                        instance);
        }

    }
    #endregion
    
    #region StatusDataWriter
    public class StatusDataWriter : DDS.OpenSplice.FooDataWriter<Status, __StatusMarshaler>, 
                                         IStatusDataWriter
    {
        public StatusDataWriter(DatabaseMarshaler marshaler)
            : base(marshaler) { }

        public InstanceHandle RegisterInstance(
                Status instanceData)
        {
            return base.RegisterInstance(
                    instanceData,
                    Time.Current);
        }

        public InstanceHandle RegisterInstanceWithTimestamp(
                Status instanceData,
                Time sourceTimestamp)
        {
            return base.RegisterInstance(
                    instanceData,
                    sourceTimestamp);
        }

        public ReturnCode UnregisterInstance(
                Status instanceData,
                InstanceHandle instanceHandle)
        {
            return base.UnregisterInstance(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode UnregisterInstanceWithTimestamp(
                Status instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.UnregisterInstance(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public ReturnCode Write(Status instanceData)
        {
            return base.Write(
                    instanceData,
                    InstanceHandle.Nil,
                    Time.Current);
        }

        public ReturnCode Write(
                Status instanceData,
                InstanceHandle instanceHandle)
        {
            return base.Write(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode WriteWithTimestamp(
                Status instanceData,
                Time sourceTimestamp)
        {
            return base.Write(
                    instanceData,
                    InstanceHandle.Nil,
                    sourceTimestamp);
        }

        public ReturnCode WriteWithTimestamp(
                Status instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.Write(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public ReturnCode Dispose(
                Status instanceData,
                InstanceHandle instanceHandle)
        {
            return base.Dispose(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode DisposeWithTimestamp(
                Status instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.Dispose(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public ReturnCode WriteDispose(
                Status instanceData)
        {
            return base.WriteDispose(
                    instanceData,
                    InstanceHandle.Nil,
                    Time.Current);
        }

        public ReturnCode WriteDispose(
                Status instanceData,
                InstanceHandle instanceHandle)
        {
            return base.WriteDispose(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode WriteDisposeWithTimestamp(
                Status instanceData,
                Time sourceTimestamp)
        {
            return base.WriteDispose(
                    instanceData,
                    InstanceHandle.Nil,
                    sourceTimestamp);
        }

        public ReturnCode WriteDisposeWithTimestamp(
                Status instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.WriteDispose(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public override ReturnCode GetKeyValue(
                ref Status key,
                InstanceHandle instanceHandle)
        {
            return base.GetKeyValue(ref key, instanceHandle);
        }

        public override InstanceHandle LookupInstance(
            Status instanceData)
        {
            return base.LookupInstance(instanceData);
        }
    }
    #endregion

    #region StatusTypeSupport
    public class StatusTypeSupport : DDS.OpenSplice.TypeSupport
    {
        private static readonly string[] metaDescriptor = {"<MetaData version=\"1.0.0\"><Module name=\"DataStructs\"><Struct name=\"Status\"><Member name=\"ResourceId\">",
"<Long/></Member><Member name=\"State\"><String/></Member><Member name=\"Info\"><String/></Member></Struct>",
"</Module></MetaData>"};

        public StatusTypeSupport()
            : base(typeof(Status), metaDescriptor, "DataStructs::Status", "", "ResourceId")
        { }


        public override ReturnCode RegisterType(IDomainParticipant participant, string typeName)
        {
            return RegisterType(participant, typeName, new __StatusMarshaler());
        }

        public override DDS.OpenSplice.DataWriter CreateDataWriter(DatabaseMarshaler marshaler)
        {
            return new StatusDataWriter(marshaler);
        }

        public override DDS.OpenSplice.DataReader CreateDataReader(DatabaseMarshaler marshaler)
        {
            return new StatusDataReader(marshaler);
        }
    }
    #endregion

    #region ExecutionDataReader
    public class ExecutionDataReader : DDS.OpenSplice.FooDataReader<Execution, __ExecutionMarshaler>, 
                                         IExecutionDataReader
    {
        public ExecutionDataReader(DatabaseMarshaler marshaler)
            : base(marshaler) { }

        public ReturnCode Read(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos)
        {
            return Read(ref dataValues, ref sampleInfos, Length.Unlimited);
        }

        public ReturnCode Read(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples)
        {
            return Read(ref dataValues, ref sampleInfos, maxSamples, SampleStateKind.Any,
                ViewStateKind.Any, InstanceStateKind.Any);
        }

        public ReturnCode Read(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            SampleStateKind sampleStates,
            ViewStateKind viewStates,
            InstanceStateKind instanceStates)
        {
            return Read(ref dataValues, ref sampleInfos, Length.Unlimited, sampleStates,
                viewStates, instanceStates);
        }

        public override ReturnCode Read(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.Read(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode Take(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos)
        {
            return Take(ref dataValues, ref sampleInfos, Length.Unlimited);
        }

        public ReturnCode Take(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples)
        {
            return Take(ref dataValues, ref sampleInfos, maxSamples, SampleStateKind.Any,
                ViewStateKind.Any, InstanceStateKind.Any);
        }

        public ReturnCode Take(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            SampleStateKind sampleStates,
            ViewStateKind viewStates,
            InstanceStateKind instanceStates)
        {
            return Take(ref dataValues, ref sampleInfos, Length.Unlimited, sampleStates,
                viewStates, instanceStates);
        }

        public override ReturnCode Take(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.Take(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode ReadWithCondition(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            IReadCondition readCondition)
        {
            return ReadWithCondition(ref dataValues, ref sampleInfos,
                Length.Unlimited, readCondition);
        }

        public override ReturnCode ReadWithCondition(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.ReadWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        readCondition);
            return result;
        }

        public ReturnCode TakeWithCondition(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            IReadCondition readCondition)
        {
            return TakeWithCondition(ref dataValues, ref sampleInfos,
                Length.Unlimited, readCondition);
        }

        public override ReturnCode TakeWithCondition(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.TakeWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        readCondition);
            return result;
        }

        public override ReturnCode ReadNextSample(
                ref Execution dataValue,
                ref SampleInfo sampleInfo)
        {
            ReturnCode result =
                base.ReadNextSample(
                        ref dataValue,
                        ref sampleInfo);
            return result;
        }

        public override ReturnCode TakeNextSample(
                ref Execution dataValue,
                ref SampleInfo sampleInfo)
        {
            ReturnCode result =
                base.TakeNextSample(
                        ref dataValue,
                        ref sampleInfo);
            return result;
        }

        public ReturnCode ReadInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return ReadInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode ReadInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return ReadInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode ReadInstance(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.ReadInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode TakeInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return TakeInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode TakeInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return TakeInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode TakeInstance(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.TakeInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode ReadNextInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return ReadNextInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode ReadNextInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return ReadNextInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode ReadNextInstance(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.ReadNextInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode TakeNextInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            InstanceHandle instanceHandle)
        {
            return TakeNextInstance(ref dataValues, ref sampleInfos, Length.Unlimited, instanceHandle);
        }

        public ReturnCode TakeNextInstance(
            ref Execution[] dataValues,
            ref SampleInfo[] sampleInfos,
            int maxSamples,
            InstanceHandle instanceHandle)
        {
            return TakeNextInstance(ref dataValues, ref sampleInfos, maxSamples, instanceHandle,
                SampleStateKind.Any, ViewStateKind.Any, InstanceStateKind.Any);
        }

        public override ReturnCode TakeNextInstance(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                SampleStateKind sampleStates,
                ViewStateKind viewStates,
                InstanceStateKind instanceStates)
        {
            ReturnCode result =
                base.TakeNextInstance(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        sampleStates,
                        viewStates,
                        instanceStates);
            return result;
        }

        public ReturnCode ReadNextInstanceWithCondition(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            return ReadNextInstanceWithCondition(
                ref dataValues,
                ref sampleInfos,
                Length.Unlimited,
                instanceHandle,
                readCondition);
        }

        public override ReturnCode ReadNextInstanceWithCondition(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.ReadNextInstanceWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        readCondition);
            return result;
        }

        public ReturnCode TakeNextInstanceWithCondition(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            return TakeNextInstanceWithCondition(
                ref dataValues,
                ref sampleInfos,
                Length.Unlimited,
                instanceHandle,
                readCondition);
        }

        public override ReturnCode TakeNextInstanceWithCondition(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos,
                int maxSamples,
                InstanceHandle instanceHandle,
                IReadCondition readCondition)
        {
            ReturnCode result =
                base.TakeNextInstanceWithCondition(
                        ref dataValues,
                        ref sampleInfos,
                        maxSamples,
                        instanceHandle,
                        readCondition);

            return result;
        }

        public override ReturnCode ReturnLoan(
                ref Execution[] dataValues,
                ref SampleInfo[] sampleInfos)
        {
            ReturnCode result =
                base.ReturnLoan(
                        ref dataValues,
                        ref sampleInfos);

            return result;
        }

        public override ReturnCode GetKeyValue(
                ref Execution key,
                InstanceHandle handle)
        {
            ReturnCode result = base.GetKeyValue(
                        ref key,
                        handle);
            return result;
        }

        public override InstanceHandle LookupInstance(
                Execution instance)
        {
            return
                base.LookupInstance(
                        instance);
        }

    }
    #endregion
    
    #region ExecutionDataWriter
    public class ExecutionDataWriter : DDS.OpenSplice.FooDataWriter<Execution, __ExecutionMarshaler>, 
                                         IExecutionDataWriter
    {
        public ExecutionDataWriter(DatabaseMarshaler marshaler)
            : base(marshaler) { }

        public InstanceHandle RegisterInstance(
                Execution instanceData)
        {
            return base.RegisterInstance(
                    instanceData,
                    Time.Current);
        }

        public InstanceHandle RegisterInstanceWithTimestamp(
                Execution instanceData,
                Time sourceTimestamp)
        {
            return base.RegisterInstance(
                    instanceData,
                    sourceTimestamp);
        }

        public ReturnCode UnregisterInstance(
                Execution instanceData,
                InstanceHandle instanceHandle)
        {
            return base.UnregisterInstance(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode UnregisterInstanceWithTimestamp(
                Execution instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.UnregisterInstance(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public ReturnCode Write(Execution instanceData)
        {
            return base.Write(
                    instanceData,
                    InstanceHandle.Nil,
                    Time.Current);
        }

        public ReturnCode Write(
                Execution instanceData,
                InstanceHandle instanceHandle)
        {
            return base.Write(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode WriteWithTimestamp(
                Execution instanceData,
                Time sourceTimestamp)
        {
            return base.Write(
                    instanceData,
                    InstanceHandle.Nil,
                    sourceTimestamp);
        }

        public ReturnCode WriteWithTimestamp(
                Execution instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.Write(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public ReturnCode Dispose(
                Execution instanceData,
                InstanceHandle instanceHandle)
        {
            return base.Dispose(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode DisposeWithTimestamp(
                Execution instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.Dispose(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public ReturnCode WriteDispose(
                Execution instanceData)
        {
            return base.WriteDispose(
                    instanceData,
                    InstanceHandle.Nil,
                    Time.Current);
        }

        public ReturnCode WriteDispose(
                Execution instanceData,
                InstanceHandle instanceHandle)
        {
            return base.WriteDispose(
                    instanceData,
                    instanceHandle,
                    Time.Current);
        }

        public ReturnCode WriteDisposeWithTimestamp(
                Execution instanceData,
                Time sourceTimestamp)
        {
            return base.WriteDispose(
                    instanceData,
                    InstanceHandle.Nil,
                    sourceTimestamp);
        }

        public ReturnCode WriteDisposeWithTimestamp(
                Execution instanceData,
                InstanceHandle instanceHandle,
                Time sourceTimestamp)
        {
            return base.WriteDispose(
                    instanceData,
                    instanceHandle,
                    sourceTimestamp);
        }

        public override ReturnCode GetKeyValue(
                ref Execution key,
                InstanceHandle instanceHandle)
        {
            return base.GetKeyValue(ref key, instanceHandle);
        }

        public override InstanceHandle LookupInstance(
            Execution instanceData)
        {
            return base.LookupInstance(instanceData);
        }
    }
    #endregion

    #region ExecutionTypeSupport
    public class ExecutionTypeSupport : DDS.OpenSplice.TypeSupport
    {
        private static readonly string[] metaDescriptor = {"<MetaData version=\"1.0.0\"><Module name=\"DataStructs\"><Struct name=\"Execution\"><Member name=\"ResourceId\">",
"<Long/></Member><Member name=\"Parameter\"><String/></Member></Struct></Module></MetaData>"};

        public ExecutionTypeSupport()
            : base(typeof(Execution), metaDescriptor, "DataStructs::Execution", "", "ResourceId")
        { }


        public override ReturnCode RegisterType(IDomainParticipant participant, string typeName)
        {
            return RegisterType(participant, typeName, new __ExecutionMarshaler());
        }

        public override DDS.OpenSplice.DataWriter CreateDataWriter(DatabaseMarshaler marshaler)
        {
            return new ExecutionDataWriter(marshaler);
        }

        public override DDS.OpenSplice.DataReader CreateDataReader(DatabaseMarshaler marshaler)
        {
            return new ExecutionDataReader(marshaler);
        }
    }
    #endregion

}

