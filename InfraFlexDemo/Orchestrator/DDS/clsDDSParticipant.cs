﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using DDS;
using DDS.OpenSplice;
using DDS.OpenSplice.CustomMarshalers;

namespace DataStructs
{
    public class clsDDSParticipant
    {
        private string _sName = "";

        private QosProvider _oQosProvider = null;
        private DomainParticipantFactory _oDPF = null;
        private IDomainParticipant _oParticipant = null;
        private IPublisher _oPublisher = null;
        private ISubscriber _oSubscriber = null;

        private Dictionary<string, ITypeSupport> _dicTypes;
        private Dictionary<string, ITopic> _dicTopics;
        private Dictionary<string, IContentFilteredTopic> _dicFilteredTopics;
        private List<string> _lstWriters;
        private List<string> _lstReaders;

        public clsDDSParticipant(string sName, string sQosFile, string sQosProfile)
        {
            // Set the name
            _sName = sName;

            // Initiate the QosProvider
            _oQosProvider = new QosProvider(sQosFile, sQosProfile);

            // Get the domain factory instance 
            _oDPF = DomainParticipantFactory.Instance;

            // Get Domain QoS
            DomainParticipantQos oDPQos = new DomainParticipantQos();

            // Get Participant QoS
            _oQosProvider.GetParticipantQos(ref oDPQos, null);
            clsErrorHandler.checkHandle(_oDPF, "DomainParticipantFactory.Instance");

            // Create participant
            _oParticipant = _oDPF.CreateParticipant(DDS.DomainId.Default, oDPQos);
            clsErrorHandler.checkHandle(_oParticipant, "DomainParticipantFactory.CreateParticipant");

            // Initiate type and topic dictionarys
            _dicTypes = new Dictionary<string, ITypeSupport>();
            _dicTopics = new Dictionary<string, ITopic>();
            _dicFilteredTopics = new Dictionary<string, IContentFilteredTopic>();
            _lstReaders = new List<string>();
            _lstWriters = new List<string>();
        }

        public IDataReader addReader(string sTopic, ITypeSupport oType, clsListener oListener, string sFilter = "", string[] arrsParameters = null)
        {
            try
            {
                // Create topic
                ITopicDescription oTopic = createTopic(sTopic, oType, sFilter, arrsParameters);

                // Check if it has a filter
                if (sFilter.Length > 0)
                {
                    IContentFilteredTopic oFilteredTopic = null;
                    _dicFilteredTopics.TryGetValue(sTopic + "Filtered", out oFilteredTopic);
                    oTopic = oFilteredTopic;
                }

                // Create subscriber
                if (_oSubscriber == null)
                    createSubscriber();

                // Create datareader
                createReader(oTopic, oListener, StatusKind.DataAvailable);

                return _oSubscriber.LookupDataReader(sTopic);
            }
            catch (Exception oEx) 
            {
                Console.Write(oEx);
                return null;
            } 
        }

        public IDataWriter addWriter(string sTopic, TypeSupport oTypeSupport) 
        {
            try
            {
                // Create the topic
                ITopic oTopic = createTopic(sTopic, oTypeSupport);

                // Create publisher
                if (_oPublisher == null)
                    createPublisher();

                // Create datawriter
                createWriter(oTopic);

                return _oPublisher.LookupDataWriter(sTopic);
            }
            catch (Exception oEx)
            {
                Console.WriteLine(oEx.Message);
                return null;
            }
        }

        public void destroy()
        {
            // Delete all the stuff to clear memory
            if (_oSubscriber != null)
            {
                // Delete readers
                _oSubscriber.DeleteContainedEntities();

                // Delete subscribers
                _oParticipant.DeleteSubscriber(_oSubscriber);
            }

            if (_oPublisher != null)
            {
                // Delete writers
                _oPublisher.DeleteContainedEntities();

                // Delete publishers
                _oParticipant.DeletePublisher(_oPublisher);
            }

            // Delete all filtered topics
            foreach (string sFilteredTopic in _dicFilteredTopics.Keys)
                deleteFilteredTopic(sFilteredTopic);

            // Delete topics
            foreach (string sTopicName in _dicTopics.Keys)
                deleteTopic(sTopicName);

            // Delete participant
            if (_oParticipant != null)
            {
                ReturnCode oStatus = _oDPF.DeleteParticipant(_oParticipant);
                clsErrorHandler.checkStatus(oStatus, "DomainParticipantFactory.DeleteParticipant");
            }
        }

        private ITopic createTopic(String sTopicName, ITypeSupport oTs, string sFilter = "", string[] arrsParameters = null)
        {
            ITopic oTopic;

            // Check if topic exists already and create if not 
            if (!_dicTopics.ContainsKey(sTopicName))
            {
                registerType(oTs);

                TopicQos oTopicQos = null;

                ReturnCode oStatus = _oQosProvider.GetTopicQos(ref oTopicQos, null);
                clsErrorHandler.checkStatus(oStatus, "DomainParticipant.GetTopicQos");

                oStatus = _oParticipant.SetDefaultTopicQos(oTopicQos);
                clsErrorHandler.checkStatus(oStatus, "DomainParticipant.SetDefaultTopicQos");

                // Create the topic
                oTopic = _oParticipant.CreateTopic(
                        sTopicName,
                        oTs.TypeName,
                        oTopicQos);

                clsErrorHandler.checkHandle(oTopic, "DomainParticipant.CreateTopic");
                _dicTopics.Add(sTopicName, oTopic);

                if (sFilter.Length > 0 && !_dicTopics.ContainsKey(sTopicName + "Filtered")) 
                {
                    IContentFilteredTopic oFilteredTopic = _oParticipant.CreateContentFilteredTopic(sTopicName + "Filtered", oTopic, sFilter, arrsParameters);
                    _dicFilteredTopics.Add(sTopicName + "Filtered", oFilteredTopic); 
                }
            }
            else
               _dicTopics.TryGetValue(sTopicName, out oTopic);

            return oTopic;
        }

        private void registerType(ITypeSupport oTs)
        {
            if (!_dicTypes.ContainsKey(oTs.TypeName))
            {
                string sTypeName = oTs.TypeName;
                ReturnCode oStatus = oTs.RegisterType(_oParticipant, sTypeName);
                clsErrorHandler.checkStatus(oStatus, "ITypeSupport.RegisterType");
                _dicTypes.Add(oTs.TypeName, oTs);
            }
        }

        private void deleteTopic(string sTopicName)
        {
            ITopic oTopic = null;
            _dicTopics.TryGetValue(sTopicName, out oTopic);

            if (oTopic != null)
                deleteTopic(oTopic);
        }

        private void deleteTopic(ITopic oTopic)
        {
            ReturnCode oStatus = _oParticipant.DeleteTopic(oTopic);
            clsErrorHandler.checkStatus(oStatus, "DDS.DomainParticipant.DeleteTopic");
        }

        private void deleteFilteredTopic(string sFilteredTopicName)
        {
            IContentFilteredTopic oTopic = null;
            _dicFilteredTopics.TryGetValue(sFilteredTopicName, out oTopic);

            if (oTopic != null)
                deleteFilteredTopic(oTopic);
        }

        private void deleteFilteredTopic(IContentFilteredTopic oTopic)
        {
            ReturnCode oStatus = _oParticipant.DeleteContentFilteredTopic(oTopic);
            clsErrorHandler.checkStatus(oStatus, "DDS.DomainParticipant.DeleteFilteredTopic");
        }

        private void createSubscriber() 
        {
            SubscriberQos oSubscriberQos = null;
            ReturnCode oStatus = _oQosProvider.GetSubscriberQos(ref oSubscriberQos, null);
            clsErrorHandler.checkStatus(oStatus, "DomainParticipant.GetSubscriberQos");

            _oSubscriber = _oParticipant.CreateSubscriber(oSubscriberQos);
            clsErrorHandler.checkHandle(_oSubscriber, "DomainParticipant.CreateSubscriber");
        }

        private void deleteSubscriber()
        {
            ReturnCode oStatus = _oParticipant.DeleteSubscriber(_oSubscriber);
            clsErrorHandler.checkStatus(oStatus, "Participant.DeleteSubscriber");
        }

        private void createPublisher()
        {
            PublisherQos oPublisherQos = null;
            ReturnCode oStatus = _oQosProvider.GetPublisherQos(ref oPublisherQos, null);
            clsErrorHandler.checkStatus(oStatus, "DomainParticipant.GetPublisherQos");

            _oPublisher = _oParticipant.CreatePublisher(oPublisherQos);
            clsErrorHandler.checkHandle(_oPublisher, "DomainParticipant.CreatePublisher");
        }

        private void deletePublisher()
        {
            ReturnCode oStatus = _oParticipant.DeletePublisher(_oPublisher);
            clsErrorHandler.checkStatus(oStatus, "DomainParticipant.DeletePublisher");
        }

        private void createWriter(ITopic oTopic)
        {
            if (!_lstWriters.Contains(oTopic.Name))
            {
                DataWriterQos oDataWriterQos = null;
                ReturnCode oStatus = _oQosProvider.GetDataWriterQos(ref oDataWriterQos, null);
                clsErrorHandler.checkStatus(oStatus, "Publisher.GetDataWriterQoS");

                IDataWriter oWriter = _oPublisher.CreateDataWriter(oTopic, oDataWriterQos);
                clsErrorHandler.checkHandle(oWriter, "Publisher.CreateDataWriter");
                _lstWriters.Add(oTopic.Name);
            }
        }

        private void createReader(ITopicDescription oTopic, clsListener oListener, StatusKind oStatusKind)
        {
            if (!_lstReaders.Contains(oTopic.Name))
            {
                DataReaderQos oDataReaderQos = null;
                ReturnCode oStatus = _oQosProvider.GetDataReaderQos(ref oDataReaderQos, null);
                clsErrorHandler.checkStatus(oStatus, "Subscriber.GetDataReaderQoS");

                IDataReader oReader = _oSubscriber.CreateDataReader(oTopic, oDataReaderQos, oListener, oStatusKind);
                clsErrorHandler.checkHandle(oReader, "Subscriber.CreateDataReader");

                _lstReaders.Add(oTopic.Name);
            }
        }
    }

    public class clsListener : DDS.IDataReaderListener
    {
        public delegate void dataReaderListenerEvent(IDataReader oReader);

        public event dataReaderListenerEvent OnRequestedDeadlineMissedHandler;
        public event dataReaderListenerEvent OnRequestedIncompatibleQosHandler;
        public event dataReaderListenerEvent OnSampleRejectedHandler;
        public event dataReaderListenerEvent OnLivelinessChangedHandler;
        public event dataReaderListenerEvent OnDataAvailableHandler;
        public event dataReaderListenerEvent OnSubscriptionMatchedHandler;
        public event dataReaderListenerEvent OnSampleLostHandler;        

        public clsListener() { }

        public void OnRequestedDeadlineMissed(DDS.IDataReader oReader, DDS.RequestedDeadlineMissedStatus oStatus)
        {
            OnRequestedDeadlineMissedHandler.BeginInvoke(oReader, null, null);
        }
        public void OnRequestedIncompatibleQos(DDS.IDataReader oReader, DDS.RequestedIncompatibleQosStatus oStatus)
        {
            OnRequestedIncompatibleQosHandler.BeginInvoke(oReader, null, null);
        }
        public void OnSampleRejected(DDS.IDataReader oReader, DDS.SampleRejectedStatus oStatus)
        {
            OnSampleRejectedHandler.BeginInvoke(oReader, null, null);
        }
        public void OnLivelinessChanged(DDS.IDataReader oReader, DDS.LivelinessChangedStatus oStatus)
        {
            OnLivelinessChangedHandler.BeginInvoke(oReader, null, null);
        }
        public void OnDataAvailable(DDS.IDataReader oReader)
        {
            OnDataAvailableHandler.BeginInvoke(oReader, null, null);
        }
        public void OnSubscriptionMatched(DDS.IDataReader oReader, DDS.SubscriptionMatchedStatus oStatus)
        {
            OnSubscriptionMatchedHandler.BeginInvoke(oReader, null, null);
        }
        public void OnSampleLost(DDS.IDataReader oReader, DDS.SampleLostStatus oStatus)
        {
            OnSampleLostHandler.BeginInvoke(oReader, null, null);
        }
    }

    /// <summary>
    /// Utility class to check the status of various Vortex OpenSplice operations.
    /// </summary>
    public sealed class clsErrorHandler
    {

        public const int NR_ERROR_CODES = 13;

        /* Array to hold the names for all ReturnCodes. */
        public static string[] RetCodeName = new string[NR_ERROR_CODES];
        public static int t;

        static clsErrorHandler()
        {
            RetCodeName[0] = "DDS_RETCODE_OK";
            RetCodeName[1] = "DDS_RETCODE_ERROR";
            RetCodeName[2] = "DDS_RETCODE_UNSUPPORTED";
            RetCodeName[3] = "DDS_RETCODE_BAD_PARAMETER";
            RetCodeName[4] = "DDS_RETCODE_PRECONDITION_NOT_MET";
            RetCodeName[5] = "DDS_RETCODE_OUT_OF_RESOURCES";
            RetCodeName[6] = "DDS_RETCODE_NOT_ENABLED";
            RetCodeName[7] = "DDS_RETCODE_IMMUTABLE_POLICY";
            RetCodeName[8] = "DDS_RETCODE_INCONSISTENT_POLICY";
            RetCodeName[9] = "DDS_RETCODE_ALREADY_DELETED";
            RetCodeName[10] = "DDS_RETCODE_TIMEOUT";
            RetCodeName[11] = "DDS_RETCODE_NO_DATA";
            RetCodeName[12] = "DDS_RETCODE_ILLEGAL_OPERATION";
        }

        /**
         * Returns the name of an error code.
         **/
        public static string getErrorName(ReturnCode status)
        {
            return RetCodeName[(int)status];
        }

        /**
         * Check the return status for errors. If there is an error,
         * then terminate.
         **/
        public static void checkStatus(ReturnCode status, string info)
        {
            if (status != ReturnCode.Ok &&
                 status != ReturnCode.NoData)
            {
                System.Console.WriteLine(
                    "Error in " + info + ": " + getErrorName(status));
                System.Environment.Exit(-1);
            }
        }

        /**
         * Check whether a valid handle has been returned. If not, then terminate.
         **/
        public static void checkHandle(object handle, string info)
        {
            if (handle == null)
            {
                System.Console.WriteLine(
                    "Error in " + info + ": Creation failed: invalid handle");
                System.Environment.Exit(-1);
            }
        }

    }

}

    