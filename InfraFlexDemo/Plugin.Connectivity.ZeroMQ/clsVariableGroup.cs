﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.ZeroMQ
{
    class clsVariableGroup : PropertyChangedBase, IVariableGroup
    {
        bool IVariableGroup.Active { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        VariableGroupDataDirection IVariableGroup.DataFlowDirection => throw new NotImplementedException();

        object IVariableGroup.ExtraSettings { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IVariableGroup.ValueChangeEventsEnabled { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        event EventHandler<ValuesChangedEventArgs> IVariableGroup.ValuesChanged
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        event EventHandler<ServerOperationCompletedEventArgs> IVariableGroup.WriteToServerCompleted
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        event EventHandler<ServerOperationCompletedEventArgs> IVariableGroup.ReadFromServerCompleted
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        IValueItem IVariableGroup.AddItem(object valueItemID)
        {
            throw new NotImplementedException();
        }

        bool IVariableGroup.ReadFromServer(IEnumerable<IValueItem> itemsToRead)
        {
            throw new NotImplementedException();
        }

        void IVariableGroup.ReadFromServerAsync(IEnumerable<IValueItem> itemsToRead, object userStateIdentifier)
        {
            throw new NotImplementedException();
        }

        bool IVariableGroup.RemoveItem(IValueItem item)
        {
            throw new NotImplementedException();
        }

        bool IVariableGroup.WriteToServer(IEnumerable<IValueItem> itemsToWrite)
        {
            throw new NotImplementedException();
        }

        void IVariableGroup.WriteToServerAsync(IEnumerable<IValueItem> itemsToWrite, object userStateIdentifier)
        {
            throw new NotImplementedException();
        }
    }
}
