﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualComponents.Connectivity.Shared;
using VisualComponents.Create3D;

namespace Plugin.Connectivity.ZeroMQ
{
    [Export(typeof(IConnectionPlugin))]
    [Export(typeof(clsZeroMQComPlugin))]
    [ExportMetadata("RequiredFeatures", LicenseFeatures.Features.Communication)]
    [PartCreationPolicy(CreationPolicy.Shared)]
    class clsZeroMQComPlugin : PropertyChangedBase, IConnectionPlugin
    {
        string IConnectionPlugin.Name => throw new NotImplementedException();

        object IConnectionPlugin.ExtraSettings { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        IEnumerable<Type> IConnectionPlugin.PluginObjectPropertyTypes => throw new NotImplementedException();

        bool IConnectionPlugin.SupportsThreadPerServer => throw new NotImplementedException();

        bool IConnectionPlugin.SupportsThreadPerVariableGroup => throw new NotImplementedException();

        bool IConnectionPlugin.SupportsAsyncWrite => throw new NotImplementedException();

        bool IConnectionPlugin.SupportsAsyncRead => throw new NotImplementedException();

        IServer IConnectionPlugin.AddServer()
        {
            throw new NotImplementedException();
        }

        bool IConnectionPlugin.RemoveServer(IServer serverToRemove)
        {
            throw new NotImplementedException();
        }
    }
}
