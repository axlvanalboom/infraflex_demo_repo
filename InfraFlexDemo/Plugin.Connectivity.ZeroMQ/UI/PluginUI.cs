﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.ZeroMQ.UI
{
    [Export(typeof(IPluginUI))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    class PluginUI : IPluginUI
    {
        IServerConnectionEditorUI IPluginUI.EditConnectionUI => throw new NotImplementedException();

        IConnectionPlugin IPluginUI.ParentPlugin => throw new NotImplementedException();

        Control IPluginUI.PluginConfigurationPanel => throw new NotImplementedException();

        Control IPluginUI.GetServerConfigurationPanel(IServer context)
        {
            throw new NotImplementedException();
        }

        Control IPluginUI.GetVariableGroupConfigurationPanel(IVariableGroup context)
        {
            throw new NotImplementedException();
        }
    }
}
