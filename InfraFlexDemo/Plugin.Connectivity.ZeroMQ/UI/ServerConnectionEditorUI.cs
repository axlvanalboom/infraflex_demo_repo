﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.ZeroMQ.UI
{
    class ServerConnectionEditorUI : IServerConnectionEditorUI
    {
        Control IServerConnectionEditorUI.PanelControl => throw new NotImplementedException();

        event EventHandler<ConnectionTestCompletedEventArgs> IServerConnectionEditorUI.ConnectionTestCompleted
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        void IServerConnectionEditorUI.Cancel()
        {
            throw new NotImplementedException();
        }

        IServerConnectionSettings IServerConnectionEditorUI.Finish()
        {
            throw new NotImplementedException();
        }

        void IServerConnectionEditorUI.Start(IServerConnectionSettings initialSettings)
        {
            throw new NotImplementedException();
        }

        void IServerConnectionEditorUI.TestConnectionAsync()
        {
            throw new NotImplementedException();
        }
    }
}
