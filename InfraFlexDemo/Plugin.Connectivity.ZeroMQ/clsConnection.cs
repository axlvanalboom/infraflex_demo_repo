﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualComponents.Connectivity.Shared;

namespace Plugin.Connectivity.ZeroMQ
{
    class clsConnection : PropertyChangedBase, IServer, IDisposable
    {
        bool IServer.Connected => throw new NotImplementedException();

        string IServer.DisplayName => throw new NotImplementedException();

        object IServer.ExtraSettings { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        ServerInfo? IServer.ServerSpecificInfo => throw new NotImplementedException();

        event EventHandler IServer.TreeItemStructureChanged
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        event EventHandler<ConnectionStateChangeEventArgs> IServer.ConnectionStateChanged
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        event EventHandler<LogEventArgs> IServer.ServerLogEvent
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        IVariableGroup IServer.AddVariableGroup(VariableGroupDataDirection dataFlowDirection)
        {
            throw new NotImplementedException();
        }

        IEnumerable<ITreeNode> IServer.Browse()
        {
            throw new NotImplementedException();
        }

        bool IServer.Connect(IServerConnectionSettings connectionSettings)
        {
            throw new NotImplementedException();
        }

        void IServer.Disconnect()
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }

        bool IServer.RemoveVariableGroup(IVariableGroup groupToRemove)
        {
            throw new NotImplementedException();
        }

        bool IServer.Reset()
        {
            throw new NotImplementedException();
        }

        bool IServer.Start()
        {
            throw new NotImplementedException();
        }

        bool IServer.Stop()
        {
            throw new NotImplementedException();
        }
    }
}
